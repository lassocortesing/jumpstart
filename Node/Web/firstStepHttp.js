const http = require("http");

http
  .createServer((req, res) => {
    res.writeHead(200, { "Content-type": "text/plain" });
    const x = {
      id: 1,
      nombre: 'Alex',
    };
    res.write(JSON.stringify(x));
    res.end();
  })
  .listen(8080);

console.log("REspuesta 8080");
