require("dotenv").config();
const express = require("express");
const hbs = require("hbs");

const app = express();
const port = process.env.PORT;

//Handlebars
app.set("view engine", "hbs");
//Partials
hbs.registerPartials(__dirname + "/views/partials", function (err) {});

// Contenido statico
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.render("home", { owner: "Ander Lacort", titulo: "Curso Node" });
});

app.get("/generic", (req, res) => {
  res.render("generic", { owner: "Ander Lacort", titulo: "Curso Node" });
  //res.sendFile(__dirname + "/public/generic.html");
});

app.get("/elements", (req, res) => {
  res.render("elements", { owner: "Ander Lacort", titulo: "Curso Node" });
  //res.sendFile(__dirname + "/public/elements.html");
});

app.get("*", (req, res) => {
  res.sendFile(__dirname + "/public/404.html");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
