const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/get", (req, res) => {
  res.send("Get");
});

app.get("*", (req, res) => {
  res.send("404 - Page not found");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
