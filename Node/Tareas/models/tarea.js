const { v4: uuidv4 } = require("uuid");
class Tarea {
  id = "";
  desc = "";
  completadoEn = null;
  constructor(_desc) {
    this.id = uuidv4();
    this.desc = _desc;
  }
}

module.exports = Tarea;
