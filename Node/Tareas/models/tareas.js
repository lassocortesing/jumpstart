const Tarea = require("./tarea");

class Tareas {
  //such as obj
  _listado = {};

  constructor() {
    this._listado = {};
  }

  crearTarea(desc = "") {
    const tarea = new Tarea(desc);
    this._listado[tarea.id] = tarea;
  }

  get lista() {
    const list = [];
    Object.keys(this._listado).forEach((key) => {
      list.push(this._listado[key]);
    });
    return list;
  }

  listadoTotal() {
    this.lista.forEach((item, i) => {
      const id = `${i + 1}. `.green;
      const { desc, completadoEn } = item;
      const state = completadoEn ? "Completa".green : "Pendiente".red;
      console.log(`${id} ${desc} :: ${state}`);
    });
  }
  listarPendientesCompletas(esCompleta) {
    let contador = 0;
    this.lista.forEach((item, i) => {
      const { desc, completadoEn } = item;
      const state = completadoEn
        ? `Completa el ${completadoEn}`.green
        : "Pendiente".red;

      if (esCompleta) {
        if (completadoEn) {
          contador += 1;
          const id = `${contador}. `.green;
          console.log(`${id} ${desc} :: ${state}`);
        }
      } else {
        if (!completadoEn) {
          contador += 1;
          const id = `${contador}. `.green;
          console.log(`${id} ${desc} :: ${state}`);
        }
      }
    });
  }

  deleteItem(id) {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }
  loadDataOnList(dataDB = []) {
    dataDB.forEach((item) => {
      this._listado[item.id] = item;
    });
  }

  completarTareas(lstTask = []) {
    lstTask.forEach((x) => {
      const temp = this._listado[x];
      if (!temp.completadoEn) {
        temp.completadoEn = new Date().toISOString();
      }
    });

    this.lista.forEach((y) => {
      if (!lstTask.includes(y.id)) {
        this._listado[y.id].completadoEn = null;
      }
    });
  }
}

module.exports = Tareas;
