const fs = require("fs");
const path = "./db/data.json";

const guardarDB = (data) => {
  fs.writeFileSync(path, JSON.stringify(data));
};

const leerDB = () => {
  if (!fs.existsSync(path)) {
    return null;
  }
  return JSON.parse(fs.readFileSync(path, "utf-8"));
};

module.exports = { save: guardarDB, read: leerDB };
