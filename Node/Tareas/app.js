require("colors");
const { save, read } = require("./helper/crearArchivo");
const {
  menu,
  stop,
  readInput,
  deleteTask,
  confirmarBorrado,
  mostrarListacheck,
} = require("./helper/mensajes");
const { tarea } = require("./models/tarea");
const Tareas = require("./models/tareas");
const { tareas } = require("./models/tareas");

const main = async () => {
  let opt = "";
  const tareas = new Tareas();
  const infoSaved = read();

  if (infoSaved) {
    tareas.loadDataOnList(infoSaved);
  }
  do {
    opt = await menu();
    switch (opt) {
      case "1":
        const describe = await readInput("Descripcion: ");
        tareas.crearTarea(describe);
        break;
      case "2":
        tareas.listadoTotal();
        break;
      case "3":
        tareas.listarPendientesCompletas(true);
        break;
      case "4":
        tareas.listarPendientesCompletas(false);
        break;
      case "5":
        const lstId = await mostrarListacheck(tareas.lista);
        tareas.completarTareas(lstId);
        console.log('***********',lstId)
        break;
      case "6":
        const id = await deleteTask(tareas.lista);
        if (id !== 0) {
          const confirma = await confirmarBorrado("Are you sure?");
          if (confirma) {
            tareas.deleteItem(id);
            console.log("Tarea borrada correctamente");
          }
        }

        break;
      default:
        break;
    }
    save(tareas.lista);
    await stop();
  } while (opt != "0");
};

main();
