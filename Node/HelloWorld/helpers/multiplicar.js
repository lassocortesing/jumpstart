const fs = require("fs");
const color = require("colors");

const crearArchivo = async (base = 5, listar = true, hasta = 10) => {
  try {
    let result = "";
    let file = "";
    for (let i = 1; i <= hasta; i++) {
      result += `${base}  ${"x".blue}  ${i}  ${"=".blue} ${base * i}\n`;
      file += `${base}  X  ${i}  = ${base * i}\n`;
    }
    if (listar) {
      console.log(`Tabla del ${base}`.green);
      console.log(result);
    }
    /* Crear archivo Sincrono
        
          fs.writeFile(`tabla${base}.txt`, result, (err) => {
            if (err) throw err;
            console.log("File created");
          });
    */

    //Crear archivo asyncrono
    fs.writeFileSync(`tabla${base}.txt`, file);
    return `tabla${base}.txt`;
  } catch (error) {
    throw error;
  }
};

module.exports = {
  metodoMultiplicar: crearArchivo,
};
