//Importar libs
const { metodoMultiplicar } = require("./helpers/multiplicar");
const { metogoYargs } = require("./config/yargs.js");
require("colors");

console.clear();

metodoMultiplicar(metogoYargs.b, metogoYargs.l, metogoYargs.h)
  .then((result) => console.log(result.rainbow, " creado"))
  .catch((err) => console.log(err));
