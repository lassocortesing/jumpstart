const argv = require("yargs")
  .option("b", {
    alias: "base",
    type: "number",
    demandOption: true,
    describe: "La base de la tabla de multiplicar",
  })
  .option("l", {
    alias: "listar",
    type: "boolean",
    demandOption: false,
    default: false,
    describe: "Bandera para mostrar o no la tabla en pantalla",
  })
  .option("h", {
    alias: "hasta",
    type: "number",
    demandOption: true,
    default: 10,
    describe: "Definir limite de la tabla",
  })
  .check((argv, options) => {
    if (isNaN(argv.b)) {
      throw "La base debe ser un numero";
    }
    return true;
  }).argv;

module.exports = {
  metogoYargs: argv,
};
