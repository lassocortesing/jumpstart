// Hola mundo
let name = "Alex";
console.log(name);
name = "Ander";
console.log(name);
//Funcion de flecha
const saludar = (name) => {
  return `Saludos ${name}`;
};
console.log(saludar("Ander"));

// Validar el orden de la ejecucion de Node
// Las funciones callback se encola en una fila de ejecucion, por eso se ejecutan despues del comando
// Node espera a ejecutar todo lo del hilo principal y luego ejecuta los callbacks

console.log(saludar("Inicio***************")); //1
//5
setTimeout(() => { 
  console.log("Primer Timeout");
}, 3000);
//3
setTimeout(() => { 
  console.log("Segundo Timeout");
}, 0);
//4
setTimeout(() => { 
  console.log("Tercero Timeout");
}, 0);
//2
console.log(saludar("Fin******************")); 
