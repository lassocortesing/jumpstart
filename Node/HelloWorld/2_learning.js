// diferencias entre const - var - let
// Var crea la variable en un ambito global, traspasa los bloques de codigo
// NO USAR VAR!!!

var name1 = "VAR - Wolverine";
if (true) {
  var name1 = "VAR - Magneto";
}
console.log(name1);
//Con let se respetan los bloques de codigo, el alcance de cada {}
// Crea variables de Scope
let name2 = "LET - Wolverine";
if (true) {
  let name2 = "LET - Magneto";
  console.log(name2);
}
console.log(name2);
//Una constante son mas ligereas que let => usar constantes cuando se sabe que el valor no va cambiar
const name3 = "Const - Wolverine";
if (true) {
  const name3 = "Const - Magneto";
  console.log(name3);
}
console.log(name3);
//************************************************** TEMPLATE String - Interpolacion o concatenacion de Strings
const nombre = "Deadpool";
const real = "Wade";
const concatenar = nombre + " " + real;
const template = `${nombre} ${real}`;
console.log(concatenar);
console.log(template);
//************************************ DESESTRUCTURACION DE OBJETOS*********************************************
// Extraer las propiedades de los objetos
const deadpool = {
  name: "Wade",
  lastName: "Wilson",
  power: "Regeneracion",
  getName() {
    return `${this.name} ${this.lastName} ${this.power}`;
  },
};

let { name, power } = deadpool;
console.log(name, power);
// Desestructuracion de objetos pasando como parametro de funciones
imprimir(deadpool);
function imprimir({ name, power }) {
  console.log("Desestructurar por parametros de funcions", name, power);
}
// Desestructuracion de Arreglos
const herores = ["Deadpool", "Superman", "Batman", "linternaVerde"];
const [h1, h2, , h5] = herores;
console.log("Arreglos desestructurados", h1, h5);

//*****************************************   FUNCIONES DE FLECHA   ********************************************
function sumar(a, b) {
  return a + b;
}
console.log(sumar(5, 5));

const flechaSuma = (a, b) => {
  return a + b;
};
const saludar = () => "Hello!";

console.log(flechaSuma(5, 6));
console.log(saludar());

//***********************************************   CALLBACK   **************************************************
//es una funcion que se pasa por parametro para ejecutarse al retorno
//ejemplo
const getUsuarioBtIs = (id, ejeCallback) => {
  const usuario = {
    id,
    nombre: "Ander",
  };
  setTimeout(() => {
    ejeCallback(usuario);
  }, 1500);
};
getUsuarioBtIs(10, (result) => {
  console.log("Ejemplo Callback normal : ", result);
});
//*********************************************   CALLBACK-HELL   **********************************************
const idBusqueda = 3;

const empleados = [
  { id: 1, nombre: "Alex" },
  { id: 2, nombre: "Aleja" },
  { id: 3, nombre: "Ander" },
];
const salario = [
  { id: 1, salario: 100 },
  { id: 2, salario: 200 },
];

const getEmpleado = (id, callback) => {
  const emple = empleados.find((e) => {
    return e.id == id;
  })?.nombre;
  if (emple) {
    callback(null, emple);
  } else {
    callback(`[CallbackHell] - Empleado con ID ${id} no existe`);
  }
};

const getSalario = (id, callback2) => {
  const sala = salario.find((s) => {
    return s.id === id;
  })?.salario;

  if (sala) {
    callback2(null, sala);
  } else {
    callback2(`[CallbackHeel] - Salario con ID ${id} no existe`);
  }
};

getEmpleado(idBusqueda, (error, resultEmpleado) => {
  if (error) {
    console.log("[CallbackHell] - ERROR!");
    return console.log(error);
  }

  getSalario(idBusqueda, (error, resultSalario) => {
    if (error) {
      return console.log("[CallbackHell] - Error en la busqueda del salario");
    }
    console.log(
      "[CallbackHell] - El epmpleado",
      resultEmpleado,
      " tiene un salario de ",
      resultSalario
    );
  });
});
//*******************************************   PROMESAS    *********************************************** */
const getEmpleadoPromesa = (id) => {
  // resolve todo OK
  // rejact para error
  return new Promise((resolve, reject) => {
    const emple = empleados.find((e) => {
      return e.id == id;
    })?.nombre;
    emple ? resolve(emple) : reject(`[Promesa] - No existe empleado con ID ${id}`);
  });
};

const getSalarioPromesa = (id) => {
  // resolve todo OK
  // rejact para error
  return new Promise((resolve, reject) => {
    const sala = salario.find((s) => {
      return s.id === id;
    })?.salario;
    sala ? resolve(sala) : reject(`[Promesa] - No existe salario con ID ${id}`);
  });
};

getEmpleadoPromesa(idBusqueda)
  .then((resultEmpleado) => console.log("[Promesa] - empleado:", resultEmpleado))
  .catch((erre) => console.log("[Promesa] - error:", erre));

getSalarioPromesa(idBusqueda)
  .then((resultSalario) => {
    console.log("[Promesa] - Salario", resultSalario);
  })
  .catch((errs) => {
    console.log("[Promesa] - error:", errs);
  });

let nameTemp;
getEmpleadoPromesa(idBusqueda)
  .then((resultEmp) => {
    nameTemp = resultEmp;
    return getSalarioPromesa(idBusqueda);
  })
  .then((resultSal) =>
    console.log(
      "[Promesa] - El epmpleado",
      nameTemp,
      " tiene un salario de ",
      resultSal
    )
  )
  .catch((err) => console.log("[Promesa] - Error ", err));

//******************************************* ASYNC - AWAIT *********************************************** */
// Transforma una funcion, en una funcion ASYN, para que retorne una promesa!!!!!!

const getAsyncHi = async () => {
  return "[Async] - Hi";
};
getAsyncHi().then((msg) => console.log(msg));

const getInfoUsuarios = async (idB) => {
  try {
    const empleadoProm = await getEmpleadoPromesa(idB);
    const salarioProm = await getSalarioPromesa(idB);
    return `[Async] -  El epmpleado ${empleadoProm} tiene un salario de ${salarioProm}`;
  } catch (error) {
    throw error;
  }
};
getInfoUsuarios(idBusqueda)
  .then((result) => console.log("[Async] - OK", result))
  .catch((err) => console.log("[Async] - FAIL:", err));
