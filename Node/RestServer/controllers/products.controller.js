const { response, request } = require("express");
const { Product } = require("../models");

const getAllProducts = async (req = request, res = response) => {
  const { limit = 3, since = 0 } = req.query;
  const active = { state: true };

  const [total, products] = await Promise.all([
    Product.countDocuments(active),
    Product.find(active)
      .populate("user", "name")
      .populate("category", "name")
      .skip(Number(since))
      .limit(Number(limit)),
  ]);

  res.json({ msg: "GetController - getAllProducts", total, products });
};

const getProduct = async (req = request, res = response) => {
  const { id } = req.params;
  const extractProduct = await Product.findById(id)
    .populate("user", "name")
    .populate("category", "name");

  res.json({ msg: "GetController - getProduct", extractProduct });
};

const postCreateProduct = async (req = request, res = response) => {
  const { state, user, ...tempProduct } = req.body;  
  const validateProduct = await Product.findOne({name: tempProduct.name });

  if (validateProduct) {
    return res
      .status(400)
      .json({ msg: `The product ${findProductName} alredy registred` });
  }

  const data = {
    ...tempProduct,
    name: tempProduct.name.toUpperCase(),
    //usertoken is created in validateJwt
    user: req.usertoken._id,
  };
  const newProduct = new Product(data);
  await newProduct.save();

  res
    .status(201)
    .json({ msg: "PostController - postCreateProduct", newProduct });
};

const putChangeProduct = async (req = request, res = response) => {
  const { id } = req.params;
  const { state, user, ...dataProduct } = req.body;

  if (dataProduct.name) {
    dataProduct.name = dataProduct.name.toUpperCase();
  }
  //usertoken is created in validateJwt
  dataProduct.user = req.usertoken._id;
  const updateProduct = await Product.findByIdAndUpdate(id, dataProduct, {
    new: true,
  });
  res.json({ msg: "PutController - putChangeProduct", updateProduct });
};

const deleteProduct = async (req = request, res = response) => {
  const { id } = req.params;
  const deleteProduct = await Product.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );
  res
    .status(200)
    .json({ msg: "DeleteController - deleteCategory", deleteProduct });
};

module.exports = {
  getAllProducts,
  getProduct,
  postCreateProduct,
  putChangeProduct,
  deleteProduct,
};
