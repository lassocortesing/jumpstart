const { response, request } = require("express");
const Usuario = require("../models/user");
const bcryptjs = require("bcryptjs");

const getUsers = async (req = request, res = response) => {
  const { limit = 3, since = 2 } = req.query;
  const active = { state: true };

  const [total, users] = await Promise.all([
    Usuario.countDocuments(active),
    Usuario.find(active).skip(Number(since)).limit(Number(limit)),
  ]);

  res.json({ msg: "GetController - Users", total, users });
};

const postUsers = async (req = request, res = response) => {
  const { name, mail, password, role } = req.body;
  const newUser = new Usuario({ name, mail, password, role });

  //Encriptar datos
  const sal = bcryptjs.genSaltSync(10);
  newUser.password = bcryptjs.hashSync(password, sal);

  await newUser.save();
  res.json({ msg: "PostController - User", newUser });
};

const putUsers = async (req = request, res = response) => {
  //mismo nombre definido en la ruta {/:}
  const param = req.params.id;
  const { _id, password, google, ...changeUSer } = req.body;
  if (password) {
    //Encriptar datos
    const sal = bcryptjs.genSaltSync(10);
    changeUSer.password = bcryptjs.hashSync(password, sal);
  }
  const updateUser = await Usuario.findByIdAndUpdate(param, changeUSer);
  res.json({ msg: "PutController - User", updateUser });
};

const deleteUsers = async (req = request, res = response) => {
  const { id } = req.params;
  const userForDelete = await Usuario.findByIdAndUpdate(id, { state: false });
  res.json({ msg: "DeleteController - User", userForDelete });
};

module.exports = {
  getUsers,
  putUsers,
  postUsers,
  deleteUsers,
};
