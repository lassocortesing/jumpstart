const { response, request } = require("express");
const Usuario = require("../models/user");
const bcryptjs = require("bcryptjs");
const { jwt } = require("../helpers/jwt");
const { googleVerify } = require("../helpers/google-verify");

const login = async (req = request, res = response) => {
  const { mail, password } = req.body;
  try {
    const validateUser = await Usuario.findOne({ mail });
    if (!validateUser) {
      return res
        .status(400)
        .json({ msg: "Issue on the email on the login process" });
    }
    if (!validateUser.state) {
      return res
        .status(400)
        .json({ msg: "Issue on the state user on the login process" });
    }
    const validatePassword = bcryptjs.compareSync(
      password,
      validateUser.password
    );
    if (!validatePassword) {
      return res
        .status(400)
        .json({ msg: "Issue on the passwprd on the login process" });
    }
    const token = await jwt(validateUser.id);
    return res.json({ msg: "Login ", validateUser, token });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msg: "Issue on the login" });
  }
};

const googleSignIn = async (req = request, res = response) => {
  try {
    const { id_token } = req.body;
    const { name, img, mail } = await googleVerify(id_token);
    let validateUser = await Usuario.findOne({ mail });
    if (!validateUser) {
      const dataToCreateUser = {
        name,
        mail,
        password: "NA",
        img,
        google: true,
      };
      validateUser = new Usuario(dataToCreateUser);
      await validateUser.save();
    }
    if (!validateUser.state) {
      return res.status(401).json({ msg: "The user is not avilable" });
    }
    //Generate JWT
    const token = await jwt(validateUser.id);
    res.json({ msg: "Google - Login ", validateUser, token });
  } catch (error) {
    res.status(400).json({ msg: "Invalid Token googleSignIn", error });
  }
};
module.exports = { login, googleSignIn };
