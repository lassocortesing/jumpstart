const { response, request } = require("express");

const getFirst = (req = request, res = response) => {
  const query = req.query;
  res.json({ msg: "get Hello - controller", query });
};

const postFirst = (req, res = response) => {
  const body = req.body;
  res.json({ msg: "post Hello - controller", body });
};

const putFirst = (req, res = response) => {
  //mismo nombre definido en la ruta {/:}
  const param = req.params.id;
  res.json({ msg: "put Hello - controller", param });
};

const deleteFirst = (req, res = response) => {
  res.json({ msg: "delete Hello - controller" });
};

module.exports = {
  getFirst,
  putFirst,
  postFirst,
  deleteFirst,
};
