const { response, request } = require("express");
const { Category } = require("../models");

const getAllCategories = async (req = request, res = response) => {
  const { limit = 3, since = 0 } = req.query;
  const active = { state: true };

  const [total, categories] = await Promise.all([
    Category.countDocuments(active),
    Category.find(active)
      .populate("user", "name")
      .skip(Number(since))
      .limit(Number(limit)),
  ]);

  res.json({ msg: "GetController - getAllCategories", total, categories });
};

const getCategory = async (req = request, res = response) => {
  const { id } = req.params;
  const extractCategory = await Category.findById(id).populate("user", "name");

  res.json({ msg: "GetController - getCategory", extractCategory });
};

const postCreateCategory = async (req = request, res = response) => {
  const name = req.body.name.toUpperCase();
  const validateCategory = await Category.findOne({ name });

  if (validateCategory) {
    return res
      .status(400)
      .json({ msg: `The cateogry ${name} alredy registred` });
  }
//usertoken is created in validateJwt
  const data = {
    name,
    user: req.usertoken._id,
  };
  const newCategory = new Category(data);
  await newCategory.save();

  res
    .status(201)
    .json({ msg: "PostController - postCreateCategory", newCategory });
};

const putChangeCategory = async (req = request, res = response) => {
  const { id } = req.params;
  const { state, user, ...dataCategory } = req.body;
  dataCategory.name = dataCategory.name.toUpperCase();

  dataCategory.user = req.usertoken._id;
  const updateCategory = await Category.findByIdAndUpdate(id, dataCategory, {
    new: true,
  });
  res.json({ msg: "PutController - putChangeCategory", updateCategory });
};

const deleteCategory = async (req = request, res = response) => {
  const { id } = req.params;
  const deleteCategory = await Category.findByIdAndUpdate(
    id,
    { state: false },
    { new: true }
  );
  res
    .status(200)
    .json({ msg: "DeleteController - deleteCategory", deleteCategory });
};
module.exports = {
  getAllCategories,
  getCategory,
  postCreateCategory,
  putChangeCategory,
  deleteCategory,
};
