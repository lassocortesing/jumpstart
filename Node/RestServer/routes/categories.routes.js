const { Router } = require("express");
const { check } = require("express-validator");
const {
  getAllCategories,
  getCategory,
  postCreateCategory,
  putChangeCategory,
  deleteCategory,
} = require("../controllers/categories.controller");
const { validateIdCategory } = require("../helpers/db-validators");
const { validateJwt } = require("../middlewares");
const {
  validateFields,
  isAdminRole,
} = require("../middlewares/validate-fields.middelwares");

/**
 * {{url}}/apy/categories
 */
const router = Router();
// get all - public
router.get("/", getAllCategories);

// get one - public
router.get(
  "/:id",
  [
    check("id", "This ID Category is invalid for Mongo DataBase").isMongoId(),
    check("id").custom(validateIdCategory),
    validateFields,
  ],
  getCategory
);
// post create category - private token
router.post(
  "/",
  [
    validateJwt,
    check("name", "The name is require").not().isEmpty(),
    validateFields,
  ],
  postCreateCategory
);

// put update category - private token
router.put(
  "/:id",
  [
    validateJwt,
    check("name", "The category name is require").not().isEmpty(),
    check("id", "This ID Category is invalid for Mongo DataBase").isMongoId(),
    check("id").custom(validateIdCategory),
    validateFields,
  ],
  putChangeCategory
);

// put update category - Admin
router.delete(
  "/:id",
  [
    validateJwt,
    isAdminRole,
    check("id", "This ID Category is invalid for Mongo DataBase").isMongoId(),
    check("id").custom(validateIdCategory),
    validateFields,
  ],
  deleteCategory
);

module.exports = router;
