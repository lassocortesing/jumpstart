const { Router } = require("express");
const { check } = require("express-validator");
const { login, googleSignIn } = require("../controllers/auth.controller");
const {
  validateFields,
} = require("../middlewares/validate-fields.middelwares");

const router = Router();

router.post(
  "/login",
  [
    check("mail", "The mail is required").isEmail(),
    check("password", "The password is required").not().isEmpty(),
    validateFields,
  ],
  login
);

router.post(
  "/google",
  [
    //check("id_token", "The id_token - Google is required").not().isEmpty(),
    validateFields,
  ],
  googleSignIn
);
module.exports = router;
