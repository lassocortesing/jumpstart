const { Router } = require("express");
const { check } = require("express-validator");
const { validateIdCategory, validateIdProduct } = require("../helpers/db-validators");
const { validateJwt } = require("../middlewares");
const {validateFields,isAdminRole,} = require("../middlewares/validate-fields.middelwares");

const {
    getAllProducts,
    getProduct,
    postCreateProduct,
    putChangeProduct,
    deleteProduct,
  } = require("../controllers/products.controller");

/**
 * {{url}}/apy/Product
 */
 const router = Router();

 // get all - public
 router.get("/", getAllProducts);
 
 // get one - public
 router.get(
   "/:id",
   [
     check("id", "This ID Product is invalid for Mongo DataBase").isMongoId(),
     check("id").custom(validateIdProduct),
     validateFields,
   ],
   getProduct
 );
 // post create Product - private token
 router.post(
   "/",
   [
     validateJwt,
     check("name", "The name is require").not().isEmpty(),
     check("category", "The category is require").not().isEmpty(),
     check("category", "The ID category isn't Mongo ID").isMongoId(),
     check("category").custom(validateIdCategory),
     validateFields,
   ],
   postCreateProduct
 );
 
 // put update product - private token
 router.put(
   "/:id",
   [
     validateJwt,     
     check("category", "The ID category isn't Mongo ID").isMongoId(),
     check("id").custom(validateIdProduct),
     validateFields,
   ],
   putChangeProduct
 );
 
 // put update category - Admin
 router.delete(
   "/:id",
   [
     validateJwt,
     isAdminRole,
     check("id", "This ID Product is invalid for Mongo DataBase").isMongoId(),
     check("id").custom(validateIdProduct),
     validateFields,
   ],
   deleteProduct
 );
 
 module.exports = router;
 