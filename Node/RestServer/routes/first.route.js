const { Router } = require("express");
const {
  getFirst,
  putFirst,
  postFirst,
  deleteFirst,
} = require("../controllers/first.controller");

const router = Router();

router.get("/", getFirst);

router.put("/:id", putFirst);

router.post("/", postFirst);

router.delete("/", deleteFirst);

module.exports = router;
