const { Router } = require("express");
const { check } = require("express-validator");

const {
  getUsers,
  putUsers,
  postUsers,
  deleteUsers,
} = require("../controllers/users.controller");

const {
  validateRole,
  validateExistingUser,
  validateExistId,
} = require("../helpers/db-validators");

const {
  validateFields,
  isRoleRules,
  validateJwt,
} = require("../middlewares/index");

const router = Router();

router.get("/", getUsers);

router.put(
  "/:id",
  [
    check("id", "Id is invalidate").isMongoId(),
    check("id").custom(validateExistId),
    check("role").custom(validateRole),
    validateFields,
  ],
  putUsers
);

router.post(
  "/",
  [
    check("name", "The name is required").not().isEmpty(),
    check(
      "password",
      "The password is required and more than 6 characters"
    ).isLength({ min: 6 }),
    check("mail", "Issues on the mail").isEmail(),
    check("mail").custom(validateExistingUser),
    check("role").custom(validateRole),
    validateFields,
  ],
  postUsers
);

router.delete(
  "/:id",
  [
    validateJwt,
    //isAdminRole,  // solovalida que sea admin cambiardo por procedimiento general
    isRoleRules("ADMIN_ROLE", "CONFIG_ROLE"),
    check("id", "Id is invalidate").isMongoId(),
    check("id").custom(validateExistId),
    validateFields,
  ],
  deleteUsers
);

module.exports = router;
