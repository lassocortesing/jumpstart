const { Schema, model } = require("mongoose");

const CategorySchema = Schema({
  name: {
    type: String,
    required: [true, "The name is required"],
    unique: true,
  },
  state: {
    type: Boolean,
    default: true,
    required: true,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
});

CategorySchema.methods.toJSON = function () {
  const { __v, state, _id, ...cateogriesFields } = this.toObject();
  cateogriesFields.cid = _id;
  return cateogriesFields;
};

//ojo validar que el modelo debe ir en plural, en este caso ingles plural
module.exports = model("Category", CategorySchema);
