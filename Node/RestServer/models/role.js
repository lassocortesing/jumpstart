const { Schema, model } = require("mongoose");

const RoleSchema = Schema({
  role: {
    type: String,
    required: [true, "The role is required"],
  },
});
//ojo validar que el modelo debe ir en plural, en este caso ingles plural
module.exports = model("Rols", RoleSchema);
