const express = require("express");
const cors = require("cors");
const { dbConnection } = require("../db/config.db");
require("dotenv").config();

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.routePath = {
      first: "/api/first",
      auth: "/api/auth",
      user: "/api/users",
      category: "/api/category",
      product: "/api/product",
    };

    // DataBase conncection
    this.dataBaseConnection();
    //Middlewares
    this.middlewares();
    //Rutas
    this.routes();
  }

  async dataBaseConnection() {
    await dbConnection();
  }
  middlewares() {
    //Cors
    this.app.use(cors());
    //lectura del Body
    this.app.use(express.json());
    //Directorio publico - contenido static que carga index.html
    this.app.use(express.static("public"));
  }

  routes() {
    this.app.use(this.routePath.auth, require("../routes/auth.routes"));
    this.app.use(this.routePath.first, require("../routes/first.route"));
    this.app.use(this.routePath.user, require("../routes/users.routes"));
    this.app.use(this.routePath.product, require("../routes/products.routes"));
    this.app.use(this.routePath.category, require("../routes/categories.routes"));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`Example app listening at http://localhost:${this.port}`);
    });
  }
}

module.exports = Server;
