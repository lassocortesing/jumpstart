const { Schema, model } = require("mongoose");

const UserSchema = Schema({
  name: { type: String, required: [true, "The name is requiered"] },
  mail: {
    type: String,
    required: [true, "The mail is requiered"],
    unique: true,
  },
  password: {
    type: String,
    required: [true, "The password is requiered"],
  },
  img: {
    type: String,
  },
  role: {
    type: String,
    requiered: true,
    default: "USER_ROLE",
    enum: ["ADMIN_ROLE", "USER_ROLE"],
  },
  state: {
    type: Boolean,
    default: true,
  },
  google: {
    type: Boolean,
    default: false,
  },
});

UserSchema.methods.toJSON = function () {
  const { __v, password, _id, ...userFields } = this.toObject();
  userFields.uid = _id;
  return userFields;
};

module.exports = model("User", UserSchema);
