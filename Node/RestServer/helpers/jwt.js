const jwtPackage = require("jsonwebtoken");

const jwt = (uid = "") => {
  return new Promise((resolve, reject) => {
    const payload = { uid };
    jwtPackage.sign(
      payload,
      process.env.PRIVATE_KEY,
      { expiresIn: "4h" },
      (err, token) => {
        if (err) {
          console.log("JWT Error", err);
          reject("Issue generate token");
        } else {
          resolve(token);
        }
      }
    );
  });
};

module.exports = { jwt };
