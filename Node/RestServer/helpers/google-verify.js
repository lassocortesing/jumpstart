const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(process.env.GOOGLE_CLIENT);

const googleVerify = async (id_token = "") => {
  //https://oauth2.googleapis.com/tokeninfo?id_token=XYZ123
  const ticket = await client.verifyIdToken({
    idToken: id_token,
    audience: process.env.GOOGLE_CLIENT,
  });
  const { name, picture: img, email: mail } = ticket.getPayload();
  return { name, img, mail };
};

module.exports = {
  googleVerify,
};
