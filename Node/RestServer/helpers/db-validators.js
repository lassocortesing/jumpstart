const { Category, Role, Usuario, Product } = require("../models");

const validateRole = async (role = "") => {
  const roleExisted = await Role.findOne({ role });
  if (!roleExisted) {
    throw new Error(`The role ${role} is not registred on data base`);
  }
};

const validateExistingUser = async (mail = "") => {
  const mailExist = await Usuario.findOne({ mail });

  if (mailExist) {
    throw new Error(`The User mail ${mail} has been registred before`);
  }
};

const validateExistId = async (id = "") => {
  const idExist = await Usuario.findById(id);

  if (!idExist) {
    throw new Error(`The id User${id} doesnt exist`);
  }
};

const validateIdCategory = async (id = "") => {
  const idExist = await Category.findById(id);

  if (!idExist) {
    throw new Error(`The id Category ${id} doesnt exist`);
  }
};

const validateIdProduct = async (id = "") => {
  const idExist = await Product.findById(id);

  if (!idExist) {
    throw new Error(`The id Product ${id} doesnt exist`);
  }
};
module.exports = {
  validateRole,
  validateExistingUser,
  validateExistId,
  validateIdCategory,
  validateIdProduct
};
