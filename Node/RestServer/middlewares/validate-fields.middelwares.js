const { request, response } = require("express");
const { validationResult } = require("express-validator");

const validateFields = (req = request, res = response, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json(errors);
  }
  next();
};

const isAdminRole = (req = request, res = response, next) => {
  //usertoken is created in validateJwt
  if (!req.usertoken) {
    return res
      .status(500)
      .json({ msg: "validate the token catched on the middlewear" });
  }
  const { role, name } = req.usertoken;
  if (role != "ADMIN_ROLE") {
    return res.status(401).json({ msg: "This user is not ADMIN" });
  }
  next();
};

const isRoleRules = (...rols) => {
  return (req = request, res = response, next) => {
    //usertoken is created in validateJwt
    if (!req.usertoken) {
      return res
        .status(500)
        .json({ msg: "validate the token catched on the middlewear" });
    }

    if (!rols.includes(req.usertoken.role)) {
      return res
        .status(401)
        .json({ msg: `The rols required for the action are ${rols}` });
    }
    next();
  };
};

module.exports = {
  validateFields,
  isAdminRole,
  isRoleRules,
};
