const validateUserFields = require("../middlewares/validate-fields.middelwares");
const validateJWT = require("../middlewares/validate-jwt.middlewares");

module.exports = {
  ...validateUserFields,
  ...validateJWT,
};
