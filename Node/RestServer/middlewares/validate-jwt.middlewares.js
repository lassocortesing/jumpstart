const { request, response } = require("express");
const jwtPackage = require("jsonwebtoken");
const Usuario = require("../models/user");

const validateJwt = async (req = request, res = response, next) => {
  const token = req.header("x-token");
  if (!token) {
    return res.status(401).json({ msg: "You need a token authoriced" });
  }
  try {
    const { uid } = jwtPackage.verify(token, process.env.PRIVATE_KEY);
    
    //Add new propertie UserToken
    req.usertoken = await Usuario.findById(uid);
    if (!req.usertoken) {
      return res.status(401).json({ msg: "This user doesnt exist" });
    }
    if (!req.usertoken.state) {
      return res.status(401).json({ msg: "This user is unable" });
    }

    next();
  } catch (error) {
    console.log("Issue ReadToken", error);
    return res.status(401).json({ msg: "You need a token authoriced" });
  }
};

module.exports = {
  validateJwt,
};
