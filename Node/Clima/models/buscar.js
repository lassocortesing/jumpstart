const fs = require("fs");
const axios = require("axios").default;
class Buscquedas {
  historial = [];
  dbPath = "./db/database.json";

  constructor() {
    this.readDB();
  }

  async ciudad(lugar = "") {
    try {
      const instance = axios.create({
        baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${lugar}.json`,
        params: this.paramsMapbox,
      });
      const res = await instance.get();

      return res.data.features.map((place) => ({
        id: place.id,
        nombre: place.place_name_es,
        lng: place.center[0],
        lat: place.center[1],
      }));
    } catch (error) {
      return [];
    }
  }

  get paramsMapbox() {
    return {
      access_token: process.env.MAPBOX_KEY,
      cachebuster: 1623789454726,
      limit: 5,
      language: "es",
    };
  }

  async climaLugar(lat, lon) {
    try {
      const instance = axios.create({
        baseURL: `https://api.openweathermap.org/data/2.5/weather`,
        params: { ...this.paramsWeather, lat, lon },
      });
      const result = await instance.get();
      const { weather, main } = result.data;
      return {
        desc: weather[0].description,
        min: main.temp_min,
        max: main.temp_max,
        temp: main.temp,
      };
    } catch (error) {
      console.log("OpenWeather Issue - ", error.code);
    }
  }
  get paramsWeather() {
    return {
      appid: process.env.OPENWEATHER_KEY,
      units: "metric",
      lang: "es",
    };
  }

  saveHistory(place = "") {
    if (this.historial.includes(place.toLocaleLowerCase())) {
      return;
    }
    this.historial = this.historial.slice(0, 5);
    this.historial.unshift(place.toLocaleLowerCase());
    this.saveDb();
  }

  saveDb() {
    const payload = { historial: this.historial };
    fs.writeFileSync(this.dbPath, JSON.stringify(payload));
  }

  readDB() {
    if (!fs.existsSync(this.dbPath)) return;

    this.historial = JSON.parse(
      fs.readFileSync(this.dbPath, "utf-8")
    ).historial;
  }

  get historialCap() {
    return this.historial.map((x) => {
      let palabras = x.split(" ");
      palabras = palabras.map((p) => p[0].toUpperCase() + p.substring(1));
      return palabras.join(" ");
    });
  }
}

module.exports = Buscquedas;
