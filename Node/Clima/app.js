require("dotenv").config();
const {
  menu,
  stop,
  readInput,
  selectTask,
  confirmarBorrado,
  mostrarListacheck,
} = require("./helper/iConsole");
const Buscquedas = require("./models/buscar");

const main = async () => {
  let opt;
  const busqueda = new Buscquedas();
  do {
    opt = await menu();
    switch (opt) {
      case 1:
        const buscar = await readInput("Ciudad de de busqueda: ");
        const lugares = await busqueda.ciudad(buscar);
        const idSelected = await selectTask(lugares);

        if (idSelected === 0) continue;

        const { id, nombre, lng, lat } = lugares.find(
          (f) => f.id === idSelected
        );

        busqueda.saveHistory(nombre);
        const { desc, min, max, temp } = await busqueda.climaLugar(lat, lng);
        console.log(
          "-----------------------------------------------------------------"
            .green
        );
        console.log("ID: ".green, id);
        console.log("Nombre: ".green, nombre);
        console.log("longitud: ".green, lng);
        console.log("latitud: ".green, lat);
        console.log("Estado: ".green, desc);
        console.log("Temp min: ".green, min);
        console.log("Temp max: ".green, max);
        console.log("Temperatura: ".green, temp);

        break;
      case 2:
        busqueda.historialCap.forEach((x, i) => {
          const idx = `${i + 1}.`.green;
          console.log(`${idx} ${x}`);
        });
        break;
      default:
        break;
    }
    if (opt !== 0) await stop();
  } while (opt !== 0);
};
main();
