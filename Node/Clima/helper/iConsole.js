const inquirer = require("inquirer");
require("colors");

const menuOpt = [
  {
    type: "list",
    name: "opcions",
    message: "Seleccione una",
    choices: [
      { value: 1, name: `${"1.".green} Buscar ciudad` },
      { value: 2, name: `${"2.".green} Historial` },
      { value: 0, name: `${"0.".green} Salir` },
    ],
  },
];
const mostrarMenu = async () => {
  console.clear();
  console.log("=============================".green);
  console.log("Seleccione una opcion");
  console.log("=============================\n".green);
  const { opcions } = await inquirer.prompt(menuOpt);

  return opcions;
};

const pausa = async () => {
  const question = [
    {
      type: "input",
      name: "Enter",
      message: `Presione ${"enter".green} para continuar`,
    },
  ];
  console.log("\n");
  await inquirer.prompt(question);
};

const leerInput = async (message) => {
  const question = [
    {
      type: "input",
      name: "desc",
      message,
      validate(value) {
        if (value.length === 0) {
          return "Debe ingresar un valor";
        }
        return true;
      },
    },
  ];
  const { desc } = await inquirer.prompt(question);
  return desc;
};

const listarItems = async (places = []) => {
  const choices = places.map((x, i) => {
    return { value: x.id, name: `${i + 1} ${x.nombre}`.green };
  });
  choices.unshift({ value: 0, name: "- Cancelar" });
  const question = [
    {
      type: "list",
      name: "id",
      message: "Seleccionar",
      choices,
    },
  ];
  const { id } = await inquirer.prompt(question);
  return id;
};

const confirmarBorrado = async (confirm) => {
  const question = [
    {
      type: "confirm",
      name: "ok",
      message: confirm,
    },
  ];
  const { ok } = await inquirer.prompt(question);
  return ok;
};

const mostrarListacheck = async (listaTarea = []) => {
  const choices = listaTarea.map((x, i) => {
    return {
      value: x.id,
      name: `${i + 1} ${x.desc}`.green,
      checked: x.completadoEn ? true : false,
    };
  });
  const question = [
    {
      type: "checkbox",
      name: "ids",
      message: "Seleccione",
      choices,
    },
  ];
  const { ids } = await inquirer.prompt(question);
  return ids;
};
module.exports = {
  menu: mostrarMenu,
  stop: pausa,
  readInput: leerInput,
  selectTask: listarItems,
  confirmarBorrado,
  mostrarListacheck,
};
