export const heroesList = [
    {
        id:1,
        name:'Batman',
        owner:'DC'
    },
    {
        id:2,
        name:'Spider-man',
        owner:'Marvel'
    },
    {
        id:3,
        name:'Superman',
        owner:'DC'
    },
];