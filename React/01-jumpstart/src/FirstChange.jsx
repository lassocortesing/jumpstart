import React from 'react';
import PropTypes from "prop-types";

// If the functions don't have dependency with the FuncComponent
// It's better let it out of FuncComponent. Example: 
const getWord=() =>{ return "Hello world!";}

export const FirstChange = ({ prpWords, defaultOwnProps }) => {
  console.log("props into the component", prpWords, defaultOwnProps);
  
  // React can not render object in, just native values html
  // but, React can show each object property  
  const objWords = {
    word: 'Say something',
    number: 123,
  };
  return (
    <>
    <h1>FirstChange New World</h1>
    <h1>React</h1>
      <p>{getWord()}</p>
      <p data-testid="test-obj-prop">{objWords.word}</p>
      <h2>{prpWords}</h2>
    </>
  )
}

// With PropTypes, We could validate the prop required !!
FirstChange.propTypes = {
  prpWords: PropTypes.string.isRequired,
};

FirstChange.defaultProps = {
  defaultOwnProps: (PropTypes.string = " default!"),
};
export default FirstChange;