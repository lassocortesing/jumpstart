import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";

// Functional Component
export const CounterApp = ({ initialValue }) => {
  const [counter, setCounter] = useState(initialValue);

  //funcion de flecha - row function
  const handledActionButtonPlus = () => {
    //Not mute the State value directly, use the method set for that!!!!, like this
    //setCounter(counter + 1); other way to access the value
    setCounter((c) => c + 1);
  };

  const handledActionButtonMinus = () => {
    //Not mute the State value directly, use the method set for that!!!!, like this
    setCounter(counter - 1);
    //other way to access the value
    //setCounter(c => c+1);
  };

  const handledActionButtonReset = () => {
    setCounter(initialValue);
  };

  console.log("Counter App value like props", initialValue);
  return (
    <Fragment>
      <h1>Welcome to Counter App</h1>
      <p>Value sent: {initialValue}</p>
      <p>Value on State: {counter}</p>
      {/*<button onClick={(e) => handledActionButton(e)}>plus +1</button>*/}
      <button onClick={handledActionButtonPlus}>plus +1</button>
      <button onClick={handledActionButtonMinus}>minus -1</button>
      <button onClick={handledActionButtonReset}>reset</button>
    </Fragment>
  );
};

// validate the prop required !!
CounterApp.propTypes = {
  initialValue: PropTypes.number.isRequired,
};

export default CounterApp;
