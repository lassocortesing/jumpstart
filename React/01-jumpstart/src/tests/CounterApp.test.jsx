import {fireEvent, render, screen} from '@testing-library/react';
import {CounterApp} from '../CounterApp';

// JUST works if the Flag "isEnable" {index} is in TRUE
describe("CounterApp component", () => {
  const initialNumber = 10;

  test("Should first load - snapshot ", () => {
    //update the snapshot: key u - during the test execution.
    const {container} = render(<CounterApp initialValue={initialNumber}/>);
    expect (container).toMatchSnapshot();
  });

  test("Should show the initial value", () => {    
    render(<CounterApp initialValue={initialNumber}/>);
    expect(screen.getByText(`Value sent: ${initialNumber}`)).toBeTruthy();
  });

  test("Should increase +1", () => {    
    render(<CounterApp initialValue={initialNumber}/>);
    fireEvent.click(screen.getByRole('button',{name:'plus +1'}));
    expect(screen.getByText(`Value on State: ${11}`)).toBeTruthy();
  });

  test("Should minus -1", () => {    
    render(<CounterApp initialValue={initialNumber}/>);
    fireEvent.click(screen.getByText('minus -1'));
    expect(screen.getByText(`Value on State: ${9}`)).toBeTruthy();
  });

  test("Should reset counter state", () => {    
    render(<CounterApp initialValue={initialNumber}/>);
    fireEvent.click(screen.getByRole('button',{name:'plus +1'}));
    fireEvent.click(screen.getByRole('button',{name:'plus +1'}));
    fireEvent.click(screen.getByRole('button',{name:'plus +1'}));
    expect(screen.getByText(`Value on State: ${13}`)).toBeTruthy();
    fireEvent.click(screen.getByRole('button',{name:'reset'}));
    expect(screen.getByText(`Value sent: ${initialNumber}`)).toBeTruthy();
  });

});
