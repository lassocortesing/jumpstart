import "@testing-library/jest-dom";
import { getHeroByIdAsync } from "../Api/HeroePromise";

describe("Agrupador de pruebas", () => {
  test("deben ser iguales los textos ", () => {
    // Preparar datos - Arrange
    const mensajeUno = "Hola mundo";
    // ejecutar       - Act
    const mensajeDos = `Hola mundo`;
    //Validar         - Assert
    expect(mensajeUno).toBe(mensajeDos);
  });

  test("deben ser iguales los objetos ", () => {
    // Preparar datos
    const objUno = { name: "Alex", saludo: "Hola" };
    // ejecutar
    const objDos = { name: "Alex", saludo: "Hola" };
    //Validar
    //expect(objUno).toBe(objDos); -- este no funciona porque los objetos tienes diferentes instancias de memoria
    expect(objUno).toEqual(objDos);
  });

  test("deben validar arreglos", () => {
    // Preparar datos
    const arregloBase = [
      { hero: "IronMan", power: "Smarter", id: "1" },
      { hero: "Superman", power: "Kryptonite", id: "2" },
    ];
    // ejecutar
    const getHero = arregloBase.find((h) => h.id === "1");
    const notHero = arregloBase.find((h) => h.id === "4");

    //Validar
    expect(getHero).toEqual({ hero: "IronMan", power: "Smarter", id: "1" });
    expect(notHero).toBe(undefined);
  });

  test("Test - getHeroByIdAsync promise Success", (done) => {
    const id = 1;
    getHeroByIdAsync(id).then((h) => {
      expect(h).toEqual([
        {
          id: 1,
          name: "Batman",
          owner: "DC",
        },
      ]);
      done();
    });
  });

  test("Test - getHeroByIdAsync promise Fail", async () => {
    const id = 5;
    try {
      await getHeroByIdAsync(id);
    } catch (error) {
      expect(error).toEqual("404 - NOT FOUND");
    }
  });
});
