import {render, screen} from '@testing-library/react';
import {FirstChange} from '../FirstChange';

// JUST works if the Flag "isEnable" {index} is in FALSE
xdescribe("FirstChange component", () => {
  const sendProp = 'New Tittle';

  test("Should first load - snapshot ", () => {
    //update the snapshot: key u - during the test execution.
    const {container} = render(<FirstChange prpWords={sendProp}/>);
    expect (container).toMatchSnapshot();
  });

  test("Should show the tittle and the html type sent ", () => {    
    render(<FirstChange prpWords={sendProp}/>);
    expect(screen.getByText(sendProp)).toBeTruthy();
    //searching and return just 1 value
    //const h2=container.querySelector('h2'); // just works with the "container" from Render
    expect(screen.getByRole('heading',{level:2}).innerHTML).toContain(sendProp);
  });

  test("Should validate by data-testId ", () => {
    const testId='test-obj-prop';    
    render(<FirstChange prpWords={sendProp}/>);
    expect(screen.getByTestId(testId)).toBeTruthy();
    expect(screen.getByTestId(testId).innerHTML).toContain('Say something');
  });

  test("Should validate by getAllBy ", () => {
    render(<FirstChange prpWords={sendProp}/>);
    expect(screen.getAllByRole('heading',{level:1}).length).toBe(2);
  });

});
