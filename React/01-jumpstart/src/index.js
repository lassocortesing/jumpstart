import React from "react";
import ReactDOM from "react-dom";
import CounterApp from "./CounterApp";
import { FirstChange } from "./FirstChange";
import "./index.css";

const divRoot = document.querySelector("#root");
const isEnable = true;
if (isEnable) {
  ReactDOM.render(<CounterApp initialValue={10} />, divRoot);
} else {
  ReactDOM.render(<FirstChange prpWords="add words like props" />, divRoot);
}
