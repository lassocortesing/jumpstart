import {heroesList} from '../common/Heroe';

export const getHeroByIdAsync = (id) =>{
    return new Promise ((resolve, reject) =>{
        setTimeout(()=>{
            const h1 = heroesList.filter(x=> x.id === id);            
            if(h1 && h1.length!=0){
                resolve(h1);
            }
            else{
                reject('404 - NOT FOUND');
            }
        },100);
    } );
}