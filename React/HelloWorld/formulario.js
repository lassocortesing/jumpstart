class Formulario extends React.Component {
  constructor(props) {
    super();
    //controlado, esta listo para actualizar el componente con base a las propiedades
    this.state = {
      texto: this.props.texto,
      aceptado: this.props.aceptado,
    };
  }
  
  //Actualizar los valores del componente
  componentWillReciveProps(nextProps) {
    this.setState({
      texto: nextProps.texto,
      aceptado: nextProps.aceptado,
    });
  }

  handleSubmit(ev) {
    ev.preventDefault();
    this.props.onSubmit({
      texto: this.state.texto,
      aceptado: this.state.aceptado,
    });
  }

  handleText(ev) {
    this.setState({
      texto: ev.target.value,
    });
  }

  handleCheckbox(ev) {
    this.setState({
      aceptado: ev.target.checked,
    });
  }

  render() {
    return (
      <from onSubmit={this.handleSubmit.bind(this)}>
        <input
          type="text"
          placeholder="Texto"
          defaultValue="Hello"
          value={this.state.texto}
          onChange={this.handleText.bind(this)}
        />
        <br />
        <label>
          <input
            type="checkbox"
            checked={this.state.aceptado}
            onChange={this.handleCheckbox.bind(this)}
          />
          Acepto los terminos
        </label>
        <button type="submit">Guardar</button>
      </from>
    );
  }
}
