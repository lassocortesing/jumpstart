class Toogle extends React.Component {
  constructor(props) {
    super();
    this.state = { activo: true };
  }

  handleClick(ev) {
    this.setState({
      activo: !this.state.activo,
    });
  }

  render() {
    return React.createElement(
      "div",
      { onClick: this.handleClick.bind(this) },
      "Activo ",
      this.state.activo ? "Si" : "NO"
    );
  }
}

var nodoToo = document.getElementById("too");
ReactDOM.render(React.createElement(Toogle,null),nodoToo);
