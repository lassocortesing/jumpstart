class Contador extends React.Component {
  constructor(props) {
    super();
    this.state = { cuenta: 0 };
  }
  render() {
    return React.createElement(
      "div",
      null,
      React.createElement("p", null, "New Component The number is: ", this.props.cuenta),
      React.createElement("input", {
        type: "button",
        value: "Inc",
        onClick: this.props.onIncrementar,
      }),
      React.createElement("input", {
        type: "button",
        value: "Dec",
        onClick: this.props.onDecrementar,
      })
    );
  }
}

var nodeContar = document.getElementById("contar");
render(0);

function render(cuenta) {
  ReactDOM.render(
    React.createElement(Contador, {
      cuenta: cuenta,
      onIncrementar: function () {
        render(cuenta + 1);
      },
      onDecrementar: function () {
        render(cuenta - 1);
      },
    }),
    nodeContar
  );
}
