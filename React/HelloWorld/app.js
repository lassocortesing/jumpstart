// Crear o describir elementos de HTML para adicionar elemento al DOM

//React.createElement(
// Primer param> Tipo de elemento o componente  del DOM que se quiere renderizar h1 , img , p, br - HTML
// Segund param> opc > pasar atributos a los elementos, pasar el objeto con el mismo nombte del atrb

// OJO : ** Los param deben ir con los nombres de los elementos de DOM como los entiende JS
// PROPS : son parametros para crear un propio componente

// Tercer param> opc > Children o enviado como propiedad explicita en el 2 param. Puede ser
// un solo parametro o una lista de paramtros.

//Crear un elemento con REACT:

class Encabezado extends React.Component {
  render() {
    return React.createElement(
      "h1",
      null,
      "My First Site Class React Component, props param: " + this.props.nombrePagina
    );
  }
}

function Menu() {
  return React.createElement(
    "ul",
    { className: "Menu Create List with React" },
    React.createElement("li", null, "First Element"),
    React.createElement("li", null, "Its just text rendering"),
    React.createElement("li", null, "Function")
  );
}

// Function Render should be pure
// it don't have impact on the components or changes the initial props.
class Contenido extends React.Component {
  render() {
    return React.createElement(
      "div",
      { className: "contenido" },
      this.props.children
    );
  }
}

class BotonCallBack extends React.Component {
  render() {
    return React.createElement(
      "button",
      {
        type: "button",
        onClick: function (event) {
          if (this.props.habilitado) {
            this.props.onClick(event);
          }
        }.bind(this),
      },
      "Guardar"
    );
  }
}

class ListCondition extends React.Component {
  render() {
    var elements = this.props.items.map(function (item, i) {
      return React.createElement("p", { key: i }, item);
    });

    if (elements.length === 0) {
      elements = this.props.renderSinResultados();
    }
    return React.createElement("div", null, elements);
  }
}

var hello = React.createElement(
  "div",
  null,
  React.createElement("img", { src: "foo.png" }),
  React.createElement("hr"),
  React.createElement("h2", null, "Rendering button passing attributes by props"),
  React.createElement(    
    "button",
    {
      onClick: function (ev) {
        console.log("Click en el boton - Funciona como callback!");
      },
    },
    "Event like callBack"
  ),
  React.createElement("hr"),
  React.createElement(
    "h1",
    { style: { color: "red" } },
    "Rendering style passing parameter by props on React - Red tittle"
  ),
  React.createElement("hr"),
  React.createElement(
    "div",
    null,
    React.createElement(Encabezado, { nombrePagina: " - Page 1" }),
    React.createElement(Menu),
    React.createElement(
      Contenido,
      null,
      React.createElement("p", null, "My content loading by Class-Children - Search className=contenido on f12")
    ),
    React.createElement(BotonCallBack, {
      habilitado: true, // Property to enable or disable the action button
      onClick: function (event) {
        console.log("Property [habilitado] in TRUE");
      },
    }),
    React.createElement(ListCondition, {
      items: ["Pass initial item on list", "second element", "initial properties"],
      renderSinResultados: function () {
        return React.createElement("p", null, "Empty!!!");
      },
    }),
    React.createElement("hr"),
    React.createElement(ListCondition, {
      items: [],
      renderSinResultados: function () {
        return React.createElement("p", null, "In this case, the items are empty [], so the code eject the els method");
      },
    })
  )
);

// Render, renderiza la UI junto con dos parametros:
// La descripcion del componente visual que se debe mostrar
// El elemento visual que va a contener el nuevo componente
ReactDOM.render(hello, document.getElementById("content"));
