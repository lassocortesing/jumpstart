import React, { useState } from "react";
import { AddCategory, GifGrid } from "./components";

export const GifExpertApp = () => {
  const [categories, setCatagories] = useState(['']);

  const onAddCategory = (newCategory) => {
    //avoid duplicate category
    if (categories.includes(newCategory)) return;

    //the useState, let us send a callback, to re-send again all the "history" data
    setCatagories([newCategory, ...categories]);
  };

  return (
    <>
      <h2> GifApp - First App</h2>
      <AddCategory onNewCategory={onAddCategory} />
      <br />
      <hr />
      <br />
      <ol>
        {categories.map((c) => (
          <GifGrid key={c} category={c} />
        ))}
      </ol>
    </>
  );
};
