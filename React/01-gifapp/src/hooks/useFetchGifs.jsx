//Custom Hook - Gifs
import { useEffect, useState } from "react";
import { getGifApi } from "../helpers/api";

export const useFetchGifs = (category) => {
  const [state, setState] = useState({
    imagesGif: [],
    loading: true,
  });

  const getImagesApi = async () => {
    const newImg = await getGifApi(category, "5");
    setState({
      imagesGif: newImg,
      loading: false,
    });
  };

  useEffect(() => {
    getImagesApi();
    // dismount
    return () => {
      console.log("eject when the component is dismount");
    };
  }, [category]);
  // [] => it mean that just run when the component mount the firs time, no more
  // [x] => the effect will run when the 'x' change  
	// nothing => trigger it each mount
	
  return state;
};
