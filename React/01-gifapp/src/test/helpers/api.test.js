import { getGifApi } from "../../helpers/api";
describe("API test", () => {
  test("should get 5 elements", async () => {
    const gifts = await getGifApi("hunter x hunter");
    expect(gifts.length).toBe(5);
    expect(gifts[0]).toEqual({
      id: expect.any(String),
      title: expect.any(String),
      url: expect.any(String),
    });
  });

  test("should not get elements", async () => {
    const gifts = await getGifApi("");
    expect(gifts.length).toBe(0);
  });
});
