import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { AddCategory } from "../../components/AddCategory";

describe("Add Category Test", () => {
  const inputCategoryExample = "Saitama";
  const mockOnNewCategory = jest.fn();

  test("Should change the category value", () => {
    render(<AddCategory onNewCategory={mockOnNewCategory} />);
    const inputCategory = screen.getByRole("textbox");
    fireEvent.input(inputCategory, { target: { value: inputCategoryExample } });
    expect(inputCategory.value).toBe("Saitama");
  });

  test("Should call the onNewCategory", () => {
    render(<AddCategory onNewCategory={mockOnNewCategory} />);

    const inputCategory = screen.getByRole("textbox");
    const formCategory = screen.getByRole("form");

    fireEvent.input(inputCategory, { target: { value: inputCategoryExample } });
    fireEvent.submit(formCategory);
    expect(inputCategory.value).toBe('');
    expect(mockOnNewCategory).toHaveBeenCalled();
    expect(mockOnNewCategory).toHaveBeenCalledTimes(1);
    expect(mockOnNewCategory).toHaveBeenCalledWith(inputCategoryExample);
  });

  test("Should not call the onNewCategory", () => {
    render(<AddCategory onNewCategory={mockOnNewCategory} />);
    const formCategory = screen.getByRole("form");
    fireEvent.submit(formCategory);
    expect(mockOnNewCategory).toHaveBeenCalledTimes(0);    
    expect(mockOnNewCategory).not.toHaveBeenCalled();
  });
});
