import React from 'react'
import {render, screen } from '@testing-library/react';
import { GifGrid } from '../../components';
import {useFetchGifs} from '../../hooks/useFetchGifs';

//Create a mock for the Hook - custom the result base on the test
jest.mock('../../hooks/useFetchGifs');

describe('GifGrid-UT', () => { 
    const testCategory = 'One punch';

    test('should show loading first step', () => {
        useFetchGifs.mockReturnValue({
            imagesGif:[],
            loading:true
        });

        render(<GifGrid category = {testCategory}/>);
        expect(screen.getByText('Loading data...')).toBeTruthy();
        expect(screen.getByText(testCategory)).toBeTruthy();
        screen.debug();
     });

     test('should load GridItems with images loaded by hook', () => {
        const gifsFixture = [
            {
                id:'1',
                title:'Goku',
                url:'https://test.com',
            },
            {
                id:'2',
                title:'Saitama',
                url:'https://test.com',
            },
            {
                id:'3',
                title:'Gon',
                url:'https://test.com',
            },
        ];
        useFetchGifs.mockReturnValue({
            imagesGif:gifsFixture,
            loading:false
        });
        render(<GifGrid category = {testCategory}/>);
        expect(screen.getAllByRole('img').length).toBe(3);
     });
 });