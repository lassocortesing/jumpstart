// import React so you can use JSX (React.createElement) in your test
import React from 'react'
/**
 * render: lets us render the component as React would = shallow
 * screen: a utility for finding elements the same way the user does
 */
import {render, screen, waitFor} from '@testing-library/react';
import { GifGridItem } from "../../components/GifGridItem";

describe('Testing GifGridItem', () => { 
    const title="Test title";
    const url="https://localhost/test.jpg";
    let containerLoaded=null;
    
    beforeEach(()=>{
        let {container} = render(<GifGridItem title={title} url={url}/>);
        containerLoaded = container;
    });

    test('should be equal screenShot', async () => {
        expect (containerLoaded).toMatchSnapshot();
     });

    test('should load img', async () => {
        let img = screen.getByRole('img');
        await waitFor(()=>{
            expect(img).toBeInTheDocument();
            //validate attribute html
            expect(img).toHaveAttribute('src',url);
            expect(img).toHaveAttribute('alt',title);            
        });        
     });

     test('should have <p> on the tittle', async () => {
        const p = screen.getByText(title);
        await waitFor(()=>{
            expect(p).toBeInTheDocument();
        });
     });

     test('should have <div> the class', async () => {
        const divClass = screen.getByTestId('ggi');
        await waitFor(()=>{
            expect(divClass).toHaveClass('animate__animated');
        });
     });
 });