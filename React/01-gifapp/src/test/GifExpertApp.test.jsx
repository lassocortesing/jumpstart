import {render, screen, waitFor, fireEvent} from '@testing-library/react';
import { GifExpertApp } from '../GifExpertApp';

describe('GifExpertApp', () => { 
    
    test('should load initial state', async() => {
        render(<GifExpertApp/>);
        const inputCategory = screen.getByRole("textbox");
        expect(screen.getByText('Loading data...')).toBeTruthy();
        expect(inputCategory.value).toBe('');
    });

    test('should finish load page', async() => {
        render(<GifExpertApp/>);
        await waitFor(
            ()=> expect(screen.queryByText('Loading data...')).not.toBeInTheDocument()
        );
        const inputCategory = screen.getByRole("textbox");
        expect(screen.queryByText('Loading data...')).toBeNull();
        expect(inputCategory.value).toBe('');
    });

    test('should load images on the page', async() => {
        render(<GifExpertApp/>);
        await waitFor(
            ()=> expect(screen.queryByText('Loading data...')).not.toBeInTheDocument()
        );
        const inputCategory = screen.getByRole("textbox");
        const formCategory = screen.getByRole("form");
        fireEvent.input(inputCategory, { target: { value: 'Goku' } });        
        fireEvent.submit(formCategory);
        await waitFor(
            ()=> expect(screen.queryByText('Loading data...')).not.toBeInTheDocument()
        );
        const imgResult = screen.getAllByTestId('ggi');

        expect(inputCategory.value).toBe('');
        expect(imgResult.length).toBe(5);
    });
 });