import { renderHook, waitFor } from "@testing-library/react";
import { useFetchGifs } from "../../hooks/useFetchGifs";

describe('Hook - useFetchGifs', () => { 

    test('should return the initial State', () => { 
        const { result } = renderHook(()=>useFetchGifs('One Punch'));
        const {imagesGif, loading} = result.current;
        expect(imagesGif.length).toBe(0);
        expect(loading).toBeTruthy();
     });

     test('should return result with data', async() => { 
        const { result } =renderHook(()=>useFetchGifs('One Punch'));
        await waitFor(
            ()=> expect(result.current.imagesGif.length).toBeGreaterThan(0)
        );
        const {imagesGif, loading} = result.current;
        expect(loading).toBeFalsy();
        expect(imagesGif.length).toBeGreaterThan(0)
     });
 });