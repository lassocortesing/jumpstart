import React, { useState } from "react";
import PropTypes from "prop-types";

export const AddCategory = ({ onNewCategory }) => {
  const [inputValue, setInputValue] = useState('');

  const handleInputChange = (e) => {
    //Para prevenir que al dar enter se refresque la pagina 
    setInputValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const newInputValue = inputValue.trim();
    if (newInputValue.length > 0) {
      onNewCategory(newInputValue);
      setInputValue('');
    }
  };

  return (
    <form onSubmit={handleSubmit} aria-label='form'>
      <input
        type="text"
        placeholder="Search gifts"
        value={inputValue}
        onChange={handleInputChange} />
    </form>
  );
};

AddCategory.propTypes = {
  onNewCategory: PropTypes.func.isRequired
};