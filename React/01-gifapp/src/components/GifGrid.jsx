import  PropTypes  from 'prop-types';
import { GifGridItem } from "./GifGridItem";
import { useFetchGifs } from "../hooks/useFetchGifs";
import React from "react";

export const GifGrid = ({ category }) => {
  const { loading, imagesGif } = useFetchGifs(category);

  return (
    <>
      {loading && <h2> Loading data...</h2>}
      <h3>{category}</h3>
      <div className="card-grid">
        <br />
        {imagesGif.map((i) => (
          <GifGridItem key={i.id} {...i} />
        ))}
      </div>
    </>
  );
};

GifGrid.prototype={
  category: PropTypes.string.isRequired,
}
