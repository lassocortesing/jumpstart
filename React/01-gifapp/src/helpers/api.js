
export const getGifApi = async (category,limit=5)=>{
    const key = "R4y4GrqPStxFanS6Hyn6Y8nkMPBXUBCY";

    const url = `https://api.giphy.com/v1/gifs/search?q=${encodeURI(category)}&limit=${limit}&api_key=${key}`;
    const result = await fetch(url);
    const { data } = await result.json();

    //Extract just the information required
    const gifToShow = data.map((img) => {
      return {
        id: img.id,
        title: img.title,
        url: img.images.downsized_medium.url,
      };
    });

    return gifToShow;
};