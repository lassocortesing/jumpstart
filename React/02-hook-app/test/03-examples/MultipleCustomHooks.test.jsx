/**  * @jest-environment jsdom  */
import { MultipleCustomHooks } from "../../src/03-examples";
import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { useFetch } from "../../src/hook/useFetch";
import { useCounter } from "../../src/hook";

//Apuntar al archivo directamente del HOOK
jest.mock('../../src/hook/useFetch');
jest.mock('../../src/hook/useCounter');

//simular Hook y llamado a API
describe('MultipleCustomHooks', () => {
  
  const mockIncrement = jest.fn();
  useCounter.mockReturnValue({
    counter: 1,
    increment: mockIncrement
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('should check the default values', () => {
    useFetch.mockReturnValue({
      data: null,
      isLoading: true,
      hasError: null
    });
    render(<MultipleCustomHooks />);
    expect(screen.getByText('BreakingBad Quotes'));
    expect(screen.getByText('Loading...'));
    const getButtonNextQuote = screen.getByRole('button', { name: 'Next quote' });
    expect(getButtonNextQuote.disabled).toBeTruthy();
  });

  test('should load the first time', () => {
    useFetch.mockReturnValue({
      data: [{ author: 'Test name', quote: 'test description' }],
      isLoading: false,
      hasError: null
    });
    render(<MultipleCustomHooks />);
    expect(screen.getByText('Test name'));
    expect(screen.getByText('test description'));
    const getButtonNextQuote = screen.getByRole('button', { name: 'Next quote' });
    expect(getButtonNextQuote.disabled).toBeFalsy();
  });

  test('should call the increment function', () => {

    useFetch.mockReturnValue({
      data: [{ author: 'Test name', quote: 'test description' }],
      isLoading: false,
      hasError: null
    });

    render(<MultipleCustomHooks />);
    const getButtonNextQuote = screen.getByRole('button', { name: 'Next quote' });
    fireEvent.click(getButtonNextQuote);
    expect(mockIncrement).toHaveBeenCalled();
  });

});