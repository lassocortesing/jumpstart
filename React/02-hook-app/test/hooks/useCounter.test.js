/**  * @jest-environment jsdom  */
import { renderHook, act } from "@testing-library/react";
import { useCounter } from "../../src/hook/useCounter";

describe("useCounter", () => {
  test("should have default values", () => {
    const { result } = renderHook(() => useCounter());
    const { counter, increment, decrement, reset } = result.current;
    expect(counter).toBe(10);
    expect(increment).toEqual(expect.any(Function));
    expect(decrement).toEqual(expect.any(Function));
    expect(reset).toEqual(expect.any(Function));
  });

  test("should set initial value", () => {
    const { result } = renderHook(() => useCounter(100));
    const { counter, increment, decrement, reset } = result.current;
    expect(counter).toBe(100);
    expect(increment).toEqual(expect.any(Function));
    expect(decrement).toEqual(expect.any(Function));
    expect(reset).toEqual(expect.any(Function));
  });

  test("should increment and decrement the initial value", () => {
    const { result } = renderHook(() => useCounter(100));
    const { increment, decrement } = result.current;    
    //Si se modifica el estado de React se debe usar ACT para que el cambio se lleve a cabo
    act(()=>{increment();});
    // Trabajar con datos primitivos, int,bool, se debe volver a extraer el resultado
    // ya que las posiciones en momria del resultado van a cambiar

    expect(result.current.counter).toEqual(101);

    act(()=>{decrement();});
    // Debido a que las funciones increment y decrement, no estan usando el valor actual enviado 
    // en los parametros del "callback", para efectos de prueba el valor inicial (en posicion de memoria)
    // siempre es el inicial. para cambiar esto se debe ajustar: 
    // const decrement = (value = 1) => {  setCounter( (current) => current - value); };
    expect(result.current.counter).toEqual(99);    
  });

  test("should reset the initial value", () => {
    const { result } = renderHook(() => useCounter(100));
    const { counter, increment, decrement, reset } = result.current;
    expect(counter).toBe(100);
    act(()=>{
      increment();
      reset();
    });
    expect(result.current.counter).toBe(100);
  });
});
