/**  * @jest-environment jsdom  */
import { renderHook, act } from "@testing-library/react";
import { urlBreakingBad } from "../../src/common/Constants";
import { useFetch } from "../../src/hook";

describe("useFetch", () => {
  const urlTest = `${urlBreakingBad}${"01"}`;

  test("should load default values ", () => {
    const { result } = renderHook(() => useFetch(urlTest));
    const { data, isLoading, hasError } = result.current;
    expect(data).toBeNull();
    expect(isLoading).toBeTruthy();
    expect(hasError).toBeNull();    
  });
});
