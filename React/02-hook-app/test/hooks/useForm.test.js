/**  * @jest-environment jsdom  */
import { renderHook, act } from "@testing-library/react";
import { useForm } from "../../src/hook/useForm";

describe("useForm", () => {
  const initialForm = {
    name: "inputName",
    email: "inputEmail",
  };
  test("should return default values", () => {
    const { result } = renderHook(() => useForm(initialForm));
    const { formState, onINputChange, onResetForm } = result.current;
    expect(formState).toEqual(initialForm);
    expect(onINputChange).toEqual(expect.any(Function));
    expect(onResetForm).toEqual(expect.any(Function));
  });

  test("should input change", () => {
    const { result } = renderHook(() => useForm(initialForm));
    const { onINputChange } = result.current;
    const event = { target: { value: "ValueChanged", name: "name" } };
    act(() => onINputChange(event));
    expect(result.current.formState).toEqual({
      name: "ValueChanged",
      email: "inputEmail",
    });
  });

  test("should reset changes", () => {
    const { result } = renderHook(() => useForm(initialForm));
    const { onINputChange, onResetForm } = result.current;
    const event = { target: { value: "ValueChanged", name: "name" } };
    act(() => onINputChange(event));
    expect(result.current.formState).toEqual({
      name: "ValueChanged",
      email: "inputEmail",
    });
    act(() => onResetForm());
    expect(result.current.formState).toEqual(initialForm);
  });
});
