import { useMemo } from "react";
import { useState } from "react";
import { useCounter } from "../hook/useCounter";



export const MemoHook = () => {

  const heavyStuff = (iterationNumber = 100) => {
    for (let i = 0; i < iterationNumber; i++) {
      console.log('Loading...');
    }
    return `${iterationNumber} - iteration`;
  };
  const { counter, increment } = useCounter(100);
  const [show, setShow] = useState(true);

  // Va a memorizar un valor, sin importar los render que haga la page
  // a menos que detecte un cambio en la depencia o el trigger config on []
  // [] solo lo memoriza la 1 vez
  // [X] permite el cambio solo si detecta un cambio en 'X'
  const memorizedValue = useMemo(() => heavyStuff(counter), [counter]);

  return (
    <>
      <h1>Counter - MemoHook <small>{counter}</small> </h1>
      <hr />
      <h4> This event just have to react with Increment, not show - {memorizedValue}</h4>

      <button
        className="btn btn-primary"
        onClick={() => increment()}>+1 Render the child component
      </button>
      <button
        className="btn btn-outline-primary"
        onClick={() => setShow(!show)}
      >
        Show/Hide {JSON.stringify(show)}  Just render the father
      </button>
    </>
  )
}
