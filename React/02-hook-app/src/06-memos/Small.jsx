// el memo ayuda a memorizar el componente
// y limita a la page a que solo se renderice 
// si las propiedades cambian!!

import { memo } from "react";

export const Small = memo(({value}) => {
  console.log('render Small');
  return (
    <small>{value}</small>
  )
})
