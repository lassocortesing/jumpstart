// Usar memo en este caso no es posible
// ya que por parametro se esta pasando una funcion
// y su posicion en memoria cambia con cada render del padre
// por lo cual se debe usar el USECALLBACK

// USECALLBACK se usa desde la funcion padre!

export const ShowIncrement = ({ increment }) => {
  console.log('Loading data again');
const functionParameter =3;
  return (
    <button
      className="btn btn-primary"
      onClick={() => { increment(functionParameter); }}>
      Incrementar
    </button>
  )
}
