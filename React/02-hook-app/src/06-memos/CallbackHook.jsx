import { useState, useCallback } from "react";
import { ShowIncrement } from "./ShowIncrement";

export const CallbackHook = () => {

  const [counter, setCounter] = useState(10); 

  // Usar memo en este caso no es posible
  // ya que por parametro se esta pasando una funcion
  // y su posicion en memoria cambia con cada render del padre
  // por lo cual se debe usar el USECALLBACK

  // USECALLBACK se usa desde la funcion padre!

  //memorizar funciones - solo se va reprocesar  cuando algo cambie
  // []   = primera vez
  // [XX] = validar dependencia para hacer render

  const incrementFather = useCallback(
    (functionParameter) => {
      //Asi no funciona el HOOK, congela todo!
      // setCounter(counter +1);
      //se Requiere enviar el callback, para que el hook no lo bloque tambien
      setCounter((callBack) => callBack + functionParameter);
    },
    [],);

  /*const incrementFather = ()=>{
    setCounter(counter+1);
  };*/

  return (
    <>
      <h1> useCallback Hook - {counter}</h1>
      <hr />
      <ShowIncrement increment={incrementFather} />
    </>
  )
}
