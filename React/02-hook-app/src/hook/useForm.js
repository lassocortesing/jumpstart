import { useState } from "react";

export const useForm = (initialForm = {}) => {
  const [formState, setFormState] = useState(initialForm);

  const onINputChange = (event) => {
    let getValue = event.target.value;
    let getField = event.target.name;
    setFormState({ ...formState, [getField]: getValue });
  };

  const onResetForm = () => {
    setFormState(initialForm);
  };

  return { ...formState, formState, onINputChange, onResetForm };
};
