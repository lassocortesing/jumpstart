import { useEffect } from "react";
import { useReducer } from "react";
import { todoReducer } from "../07-useReducer/todoReducer";

const initialState = [];

const init = () => {
  return JSON.parse(localStorage.getItem("source")) || [];
};

export const useTodo = () => {
  const [stateTodo, dispatchTodo] = useReducer(todoReducer, initialState, init);

  useEffect(() => {
    localStorage.setItem("source", JSON.stringify(stateTodo));
  }, [stateTodo]);

  const handleNewTodo = (todo) => {
    const actionAdd = {
      type: "[TODO]-ADD",
      payload: todo,
    };
    dispatchTodo(actionAdd);
  };

  const handleDeleteTodo = (todo) => {
    const actionDelete = {
      type: "[TODO]-DELETE",
      payload: todo,
    };
    dispatchTodo(actionDelete);
  };

  const handleToggleTodo = (todo) => {
    const actionDone = {
      type: "[TODO]-TOOGLE",
      payload: todo,
    };
    dispatchTodo(actionDone);
  };

  return {
    stateTodo,
    handleNewTodo,
    handleDeleteTodo,
    handleToggleTodo,
    todoCount: stateTodo.length,
    pendingTodoCount: stateTodo.filter((f) => f.done === false).length,
  };
};
