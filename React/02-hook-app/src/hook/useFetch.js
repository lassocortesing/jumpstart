import { useEffect, useState } from "react";

export const useFetch = (url) => {

  const [state, setState] = useState({
    data:null,
    isLoading:true,
    hasError:null,
  });

  const getFetch = async () => {
    setState({
      ...state,      
      isLoading:true,
      hasError:null,
    });

    const result = await fetch(url);
    const data = await result.json();
    
    setState({
      ...state,
      data,
      isLoading:false,
      hasError:null,
    });
  };

  useEffect(() => {
    getFetch();

  }, [url]);

  return {
    data:state.data,
    isLoading:state.isLoading,
    hasError:state.hasError,
  };
};
