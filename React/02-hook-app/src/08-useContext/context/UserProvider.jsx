// HOC receive the children
//Create Context

import { useState } from "react"
import { UserContext } from "./UserContext"

export const UserProvider = ({children}) => {

  const [user, setUser] = useState();

  

  return (
    //compartir informacion con los hijos, obj, variables, etc
    <UserContext.Provider value={{user,setUser}}>
      {children}
    </UserContext.Provider>
  )
}
