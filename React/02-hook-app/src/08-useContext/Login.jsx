import { useContext } from "react"
import { UserContext } from "./context/UserContext";

export const Login = () => {
  // get data from Context
  // si existen obj similares o iguales, el context obtiene el obj mas cercano
  const { user, setUser } = useContext(UserContext)
  return (
    <>
      <h1>Login - Context</h1>
      <hr />
      <pre>
        {JSON.stringify(user, null, 3)}
      </pre>
      <button className="btn btn-primary"
      onClick={()=>setUser({id:123,name:'Juan',email:'mail'})}> Set User</button>
    </>
  )
}
