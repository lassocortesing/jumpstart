import { Routes, Route, Navigate } from 'react-router-dom';
import { About } from './About';
import { UserProvider } from './context/UserProvider';
import { Home } from './Home';
import { Login } from './Login';
import { Navbar } from './Navbar';


export const MainApp = () => {
  return (
    //El proveedor de contexto debe estar por encima d elos hijos a los cuales les compartira info
    <UserProvider>
      <h1>MainApp - Context</h1>
      <Navbar/>
      <hr />
      <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='about' element={<About />}></Route>
        <Route path='login' element={<Login />}></Route>

        {/* wildCard: comodin para redireccionar la URL si el usuario manda una ruta que no existe*/}
        <Route path='/*' element={<Navigate to='/login'/>}></Route>
      </Routes>
    </UserProvider>
  )
}
