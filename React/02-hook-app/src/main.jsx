import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { MainApp } from './08-useContext/MainApp';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.createRoot(document.getElementById('root')).render(
  // Load the component required
  // <CounterCustom/>
  // <SimpleForm/>
  // <FormWithHook/>
  // <MultipleCustomHooks/>
  // <FocusScreen/>
  // <Layout/>
  // <Memorize/>
  // <MemoHook/>
  // <CallbackHook/>  
  // <Padre/>  
  // import './07-useReducer/intro-Reducer';
  // <TodoApp />

  
  // High order Component -HOC
  // Permite que todos los compenentes hijos dentro de el,
  // tienen acceso a la informacion del padre 
  <BrowserRouter>
    <React.StrictMode>
      <MainApp />
    </React.StrictMode>
  </BrowserRouter>
)
