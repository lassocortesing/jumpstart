import { useState, useRef, useLayoutEffect } from 'react';

export const Quote = ({author,quote}) => {
	const pRef = useRef();
	const [boxSize, setBoxSize] = useState({width:0,height:0});
	
	//Se recomienda usar el useEffect!
	useLayoutEffect(() => {
		//get the p size
		const {height, width} =pRef.current.getBoundingClientRect();
		setBoxSize({width,height});
		
	}, []);

	return (
		<>
		<blockquote className="blockquote text-end"
		style={{display:'flex'}}
		>
			<p className="mb-1"
			ref={pRef}>{author}</p>
			<footer className="blockquote-footer">{quote}</footer>
		</blockquote>
		<code>{JSON.stringify(boxSize)}</code>
		</>
	)
}