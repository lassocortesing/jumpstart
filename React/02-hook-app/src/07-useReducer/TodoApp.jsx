import { useTodo } from "../hook/useTodo";
import { TodoAdd } from "./TodoAdd";
import { TodoList } from "./TodoList";

export const TodoApp = () => {
  const { stateTodo, handleNewTodo, handleDeleteTodo, handleToggleTodo, todoCount, pendingTodoCount } = useTodo();
  return (
    <>
      <h1>TODO App [{todoCount}] - <small> Pending: [{pendingTodoCount}]</small></h1>
      <hr />

      <div className="row">
        <div className="col-7">
          <TodoList
            stateTodo={stateTodo}
            onDeleteTodo={handleDeleteTodo}
            onToggleTodo={handleToggleTodo} />
        </div>
        <TodoAdd onNewTodo={handleNewTodo} />
      </div>
    </>
  )
}
