import React from 'react'

export const TodoItem = ({ stateElement, onDeleteTodo, onToggleTodo }) => {
  return (
    <li className="list-group-item d-flex justify-content-between">
      <span
        className={`align-self-center ${stateElement.done ? 'text-decoration-line-through' : ''}`}
        onClick={() => onToggleTodo(stateElement)}
      >{stateElement.description}</span>
      <button
        className="btn btn-danger"
        onClick={() => onDeleteTodo(stateElement)}>Delete</button>
    </li>
  )
}
