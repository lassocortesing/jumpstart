import React from 'react'
import { TodoItem } from './TodoItem'

export const TodoList = ({ stateTodo = [], onDeleteTodo, onToggleTodo }) => {
  return (
    <ul className="list-group">
      {
        stateTodo.map(
          stateElement => (
            <TodoItem key={stateElement.id}
              stateElement={stateElement}
              onDeleteTodo={onDeleteTodo}
              onToggleTodo={onToggleTodo} />
          )
        )
      }
    </ul>
  )
}
