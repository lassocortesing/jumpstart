export const todoReducer = (initialState = [], action) => {
  switch (action.type) {
    case "[TODO]-ADD":
      return [...initialState, action.payload];
    case "[TODO]-DELETE":
      return initialState.filter((i) => i.id !== action.payload.id);
    case "[TODO]-TOOGLE":
      return initialState.map((i) => {
        if (i.id === action.payload.id) {
          return {
            ...i,
            done: !i.done,
          };
        }
        return i;
      });
    default:
      return initialState;
  }
};
