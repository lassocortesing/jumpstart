import { useForm } from "../hook/useForm"

export const TodoAdd = ({onNewTodo}) => {
  const {description, onINputChange,onResetForm} = useForm({ description:'',});

  const onFormSubmit = (e)=>{
    e.preventDefault();
    if(description.trim().length <=1) return;
    const newTodoAdd ={
      id:new Date().getTime(),
      done:false,
      description:description
    };
    onNewTodo && onNewTodo(newTodoAdd);
    onResetForm();
  }; 

  return (
    <div className="col-5">
      <h4>Add TODO</h4>
      <hr />
      <form onSubmit={onFormSubmit}>
        <input
          type='text'
          name='description'
          placeholder='Task to do'
          className="form-control"
          value={description}
          onChange={onINputChange}
        />
        <button type="submit" className="btn btn-outline-primary mt-1">Add</button>
      </form>
    </div>
  )
}
