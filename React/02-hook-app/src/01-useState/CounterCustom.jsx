import { useState } from "react";
import { useCounter } from "../hook/useCounter";

export const CounterCustom = () => {
  const { counter, increment, decrement, reset } = useCounter();
  const [inputValue, setInputValue] = useState(1);

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  return (
    <>
      <h1>Counter Custom Hook: {counter}</h1>
      <label>Number factor: </label>
      <input
        type="text"
        placeholder="Change number to increment or decrement"
        value={inputValue}
        onChange={handleInputChange} />

      <hr />
      <button className="btn btn-primary" onClick={()=>increment(inputValue)}>+1</button>
      <button className="btn btn-primary" onClick={reset}>Reset</button>
      <button className="btn btn-primary" onClick={()=>decrement(inputValue)}>-1</button>
    </>
  )
}
