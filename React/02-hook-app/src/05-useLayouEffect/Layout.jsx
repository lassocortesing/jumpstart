import { urlBreakingBad } from "../common/Constants";
import normalizeUrl from 'normalize-url';
import { useCounter, useFetch } from "../hook";
import { LoadingQuote, Quote } from "../03-examples";

export const Layout = () => {
  const { counter, increment } = useCounter(1);
  const { data, isLoading, hasError } = useFetch(normalizeUrl(`${urlBreakingBad}/quotes/${counter}`));

  //ID data have values THEN take the data on [0]
  // the negation of empty is Something  => !null = true
  // !! that mean double negation is useful to know if the object have value or id different to undefined or null
  const { author, quote } = !!data && data[0];
  return (
    <>
      <h1> Layout useLayoutEffect</h1>
      <hr />
      {isLoading ?
        <LoadingQuote />
        :
        <Quote author={author} quote={quote} />
      }
      <button
        className="btn btn-primary"
        disabled={isLoading}
        onClick={() => increment()}>Next quote</button>
    </>
  )
}
