import { useRef } from "react";

export const FocusScreen = () => {
	// Permite mantener una referencia
	// pero cuando la referencia cabia
	// no manda una Renderizacion de la page
	// variables u obj de HTML

	const inputRefFocus = useRef();

	const onFocusManual = () => {		
		//Foco
		//document.querySelector('input').focus();
		//foco y permite escribir inmediatamente
		//document.querySelector('input').select();
		inputRefFocus.current.select();
	};

	return (
		<>
			<h1>Focus Screen</h1>
			<hr />
			<input
				type='text'
				placeholder="Name"
				ref={inputRefFocus}
				className="form-control" />
			<button className="btn btn-primary mt-2"
				onClick={onFocusManual}>
				Set focus
			</button>
		</>
	)
}
