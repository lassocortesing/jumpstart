import { useState } from "react"
import { Message } from "./Message";

export const SimpleForm = () => {

	const [formState, setFormState] = useState({
		userName: 'anderLacort',
		email: 'lasso.cortes@gmail.com',
	});
	const { userName, email } = formState;
	const onINputChange = (event) => {
		let getValue = event.target.value;
		let getField = event.target.name;
		setFormState({ ...formState, [getField]: getValue });
	};

	return (
		<>
			<h1>Simple Form</h1>
			<hr />
			<input
				type='text'
				className="form-control"
				placeholder="UserName"
				name="userName"
				value={userName}
				onChange={onINputChange}
			/>
			<input
				type='email'
				className="form-control mt-2"
				placeholder="correo@mail.com"
				name="email"
				value={email}
				onChange={onINputChange}
			/>
			{
				userName ==='alex' && <Message />
			}
			
		</>
	)
}
