import { useEffect, useState } from "react"

export const Message = () => {

	const [coords, setCoords] = useState({x:0,y:0})
	useEffect(() => {
		console.log('Message Mounted');

		//example when we required clean the dependencies
		const onMouseMoveExample = ({x,y}) => {
			//move the mouse to check it
			console.log('Be careful!');
			setCoords({x,y});
		};

		//example when we required clean the dependencies
		window.addEventListener('mousemove', onMouseMoveExample);

		return () => {
			console.log('Message Unmounted');

			//Example when we required clean the dependencies
			//Even when the component was unmounted the mouseEvent continue working
			//We need required the reference and remove it
			window.removeEventListener('mousemove', onMouseMoveExample);
		}
	}, []);

	return (
		<>
			<h3>User already exist!</h3>
			{JSON.stringify(coords)}
		</>
	)
}
