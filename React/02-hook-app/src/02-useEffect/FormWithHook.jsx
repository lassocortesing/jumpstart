import { useForm } from "../hook/useForm"

export const FormWithHook = () => {

	const { userName, email, password, onINputChange, onResetForm } = useForm({
		userName: '',
		email: '',
		password: '',
	});

	// We dont need it, because we spreed the props on the hook "...formState"
	//const {userName, email, password} = formState

	return (
		<>
			<h1>Form with Hooks</h1>
			<hr />
			<input
				type='text'
				className="form-control"
				placeholder="UserName"
				name="userName"
				value={userName}
				onChange={onINputChange}
			/>
			<input
				type='email'
				className="form-control mt-2"
				placeholder="correo@mail.com"
				name="email"
				value={email}
				onChange={onINputChange}
			/>
			<input
				type='password'
				className="form-control mt-2"
				placeholder="Password"
				name="password"
				value={password}
				onChange={onINputChange}
			/>
			<button className="btn btn-primary mt-2" onClick={onResetForm}>Eraser</button>
		</>
	)
}
