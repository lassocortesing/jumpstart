module.exports = async()=>{
  // ...いろいろ設定
  return {
  testEnvironment: "jsdom",
  setupFiles: ["./jest.setup.js"],
  transform: {
    "\\.[jt]sx?$": "babel-jest",    
  },
}
};
