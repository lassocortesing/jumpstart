﻿using FirstCoreApp.Data;
using FirstCoreApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FirstCoreApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiHomeController : ControllerBase
    {
        private readonly ILogger<ApiHomeController> _logger;
        private readonly ApplicationDbContext _context;
        public ApiHomeController(ILogger<ApiHomeController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        // GET: api/<ApiHomeController>
        [HttpGet("GetAllUsers")]
        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return await _context.User.ToListAsync();
        }

        // GET api/<ApiHomeController>/5
        [HttpGet("GetUser/{id}")]
        public User GetUser(int? id)
        {
            var users = _context.User.Where(x => x.Id == id).ToList();

            if (id == null)
                return null;

            if (users == null || users.Count == 0)
                return null;

            return users.FirstOrDefault();
        }

        // POST api/<ApiHomeController>
        [HttpPost("CreateUser")]
        public async Task<string> CreateUser(User newUser)
        {
            _context.User.Add(newUser);
            await _context.SaveChangesAsync();
            return "OK - Create";
        }

        // PUT api/<ApiHomeController>/5
        [HttpPut("UpdateUser")]
        public async Task<string> UpdateUser(User editUser)
        {
            _context.Update(editUser);
            await _context.SaveChangesAsync();
            return "OK - Update";
        }

        // DELETE api/<ApiHomeController>/5
        [HttpDelete("DeleteUser/{id}")]
        public async Task<string> DeleteUser(int? id)
        {
            var userToDelete = await _context.User.FindAsync(id);
            _context.User.Remove(userToDelete);
            await _context.SaveChangesAsync();
            return "OK - Delete";
        }
    }
}
