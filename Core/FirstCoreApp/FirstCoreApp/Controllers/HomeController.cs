﻿using FirstCoreApp.Data;
using FirstCoreApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FirstCoreApp.Controllers
{

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _context.User.ToListAsync());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(User newUser)
        {
            if (ModelState.IsValid)
            {
                _context.User.Add(newUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var users = _context.User.Where(x => x.Id == id).ToList();

            if (id == null)
                return NotFound();

            if (users == null || users.Count == 0)
                return NotFound();

            return View(users.FirstOrDefault());
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(User editUser)
        {
            if (ModelState.IsValid)
            {
                _context.Update(editUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(editUser);
        }

        [HttpGet]        
        public async Task<IActionResult> Delete(int? id)
        {
            var userToDelete = await _context.User.FindAsync(id);
            if (userToDelete == null)
            {
                return NotFound();
            }
            _context.User.Remove(userToDelete);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));

        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {            

            if (id == null)
                return NotFound();

            var users = _context.User.Find(id);
            if (users == null )
                return NotFound();

            return View(users);
        }

    }
}
