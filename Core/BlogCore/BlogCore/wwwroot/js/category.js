﻿var dataTableCategory = '#tblCategories';

$(document).ready(function () {
    var tCategory = $(dataTableCategory).dataTable(
        {
            "pageLength": 10,
            "language": {
                "emptyTable": "Empty table"
            },
            "width": "100%"
        });
    loadDataTable();
});

function loadDataTable() {
    $.get('/Admin/Categories/GetAll', function (response) {
        var tempCategory = $(dataTableCategory).dataTable();
        if (response != null && response.length != 0) {
            var data = response.data;
            tempCategory._fnClearTable();

            if (data.length == 0) {
                tempCategory._fnReDraw();
            }

            $(data).each(function (i, item) {
                var addData = [];
                addData.push(`<div class="text-center"> ${item.id} </div>`);
                addData.push(`<div class="text-center"> ${item.name} </div>`);
                addData.push(`<div class="text-center"> ${item.order} </div>`);
                addData.push(`
                            <div class="text-center">
                            <a href='/Admin/Categories/Edit/${item.id}' class='btn btn-success text-white' style='cursor:pointer; width:100px;'>
                            <i class='fas fa-edit'></i> Edit
                            </a>
                            &nbsp;
                            <a onclick=Delete("/Admin/Categories/Delete/${item.id}") class='btn btn-danger text-white' style='cursor:pointer; width:100px;'>
                            <i class='fas fa-trash-alt'></i> Delete
                            </a>
                            </div>
                            `);
                tempCategory.fnAddData(addData);
            });
        }


    });
}

function Delete(url) {
    swal({
        title: "Are you sure for delete ?",
        text: "This data will not recover then",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnconfirm: true
    }, function () {
        $.post(url, function (response) {
            if (response.success) {
                toastr.success(response.message);
                loadDataTable();
            }
            else {
                toastr.error(response.message);
            }
        });
    });
}