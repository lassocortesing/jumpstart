﻿var tblItem = '#tblItem';

$(document).ready(function () {
    var tItem = $(tblItem).dataTable(
        {
            "pageLength": 10,
            "language": {
                "emptyTable": "Empty table"
            },
            "width": "100%"
        });
    loadDataTable();
});

function loadDataTable() {
    $.get('/Admin/Items/GetAll', function (response) {
        var tempItem = $(tblItem).dataTable();
        if (response != null && response.length != 0) {
            var data = response.data;
            tempItem._fnClearTable();

            if (data.length == 0) {
                tempItem._fnReDraw();
            }

            $(data).each(function (i, item) {
                var addData = [];                

                addData.push(`<div class="text-center"> ${item.id} </div>`);
                addData.push(`<div class="text-center"> ${item.name} </div>`);
                addData.push(`<div class="text-center"> ${item.category.name} </div>`);
                addData.push(`<div class="text-center"> ${item.creation.slice(0,10)} </div>`);
                addData.push(`
                            <div class="text-center">
                            <a href='/Admin/Items/Edit/${item.id}' class='btn btn-success text-white' style='cursor:pointer; width:100px;'>
                            <i class='fas fa-edit'></i> Edit
                            </a>
                            &nbsp;
                            <a onclick=Delete("/Admin/Items/Delete/${item.id}") class='btn btn-danger text-white' style='cursor:pointer; width:100px;'>
                            <i class='fas fa-trash-alt'></i> Delete
                            </a>
                            </div>
                            `);
                tempItem.fnAddData(addData);
            });
        }


    });
}

function Delete(url) {
    swal({
        title: "Are you sure for delete ?",
        text: "This data will not recover then",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnconfirm: true
    }, function () {
        $.post(url, function (response) {
            if (response.success) {
                toastr.success(response.message);
                loadDataTable();
            }
            else {
                toastr.error(response.message);
            }
        });
    });
}