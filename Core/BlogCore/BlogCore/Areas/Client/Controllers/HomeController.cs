﻿
using BlogCore.Data.Data.Repository;
using BlogCore.Models;
using BlogCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCore.Controllers
{
    [Area("Client")]
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IActionResult Index()
        {
            HomeVM home = new HomeVM()
            {
                lstSlider = _unitOfWork.Slider.GetAll(),
                lstItem = _unitOfWork.Item.GetAll()
            };
            return View(home);
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var item = _unitOfWork.Item.GetFirstOrDefault(a => a.Id == id);
            return View(item);
        }


    }
}
