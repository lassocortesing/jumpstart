﻿using BlogCore.Data.Data.Repository;
using BlogCore.Models.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;


namespace BlogCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ItemsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostingEnviroment;
        public ItemsController(IUnitOfWork unitOfWork, IWebHostEnvironment hostingEnviroment)
        {
            _unitOfWork = unitOfWork;
            _hostingEnviroment = hostingEnviroment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            ItemVM item = new ItemVM()
            {
                Item = new Models.Item(),
                lstCategories = _unitOfWork.Category.GetListCategory()
            };
            return View(item);
        }

        [HttpPost]
        public IActionResult Create(ItemVM newItem)
        {
            if (ModelState.IsValid)
            {
                string path = _hostingEnviroment.WebRootPath;
                var files = HttpContext.Request.Form.Files;

                if (newItem.Item.Id == 0)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var upload = Path.Combine(path, @"img/items");
                    var extension = Path.GetExtension(files[0].FileName);
                    using (var fileStreams = new FileStream(Path.Combine(upload, string.Concat(fileName, extension)), FileMode.Create))
                    {
                        files[0].CopyTo(fileStreams);
                    }
                    newItem.Item.UrlImage = string.Concat(@"/img/items/", fileName, extension);
                    newItem.Item.Creation = DateTime.Now;
                    _unitOfWork.Item.Add(newItem.Item);
                    _unitOfWork.Save();

                    return RedirectToAction(nameof(Index));
                }
            }
            newItem.lstCategories = _unitOfWork.Category.GetListCategory();
            return View(newItem);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            ItemVM item = new ItemVM()
            {
                Item = new Models.Item(),
                lstCategories = _unitOfWork.Category.GetListCategory()
            };
            if (id != null)
            {
                item.Item = _unitOfWork.Item.Get(id.GetValueOrDefault());
            }
            return View(item);
        }

        [HttpPost]
        public IActionResult Edit(ItemVM changeItem)
        {
            if (ModelState.IsValid)
            {
                string sourcePath = _hostingEnviroment.WebRootPath;
                var files = HttpContext.Request.Form.Files;

                var currentItem = _unitOfWork.Item.Get(changeItem.Item.Id);

                if (files.Count > 0)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var upload = Path.Combine(sourcePath, @"img/items");
                    var newExtension = Path.GetExtension(files[0].FileName);

                    var pathImage = Path.Combine(sourcePath, currentItem.UrlImage.TrimStart('/'));
                    if (System.IO.File.Exists(pathImage))
                    {
                        System.IO.File.Delete(pathImage);
                    }
                    using (var fileStreams = new FileStream(Path.Combine(upload, string.Concat(fileName, newExtension)), FileMode.Create))
                    {
                        files[0].CopyTo(fileStreams);
                    }
                    changeItem.Item.UrlImage = string.Concat(@"/img/items/", fileName, newExtension);
                    changeItem.Item.Creation = DateTime.Now;
                }
                else
                {
                    changeItem.Item.UrlImage = currentItem.UrlImage;
                }
                _unitOfWork.Item.UpdateItem(changeItem.Item);
                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var eraserItem = _unitOfWork.Item.Get(id);
            string sourcePath = _hostingEnviroment.WebRootPath;
            var pathImg = Path.Combine(sourcePath, eraserItem.UrlImage.TrimStart('/'));

            if (System.IO.File.Exists(pathImg))
            {
                System.IO.File.Delete(pathImg);
            }
            if (eraserItem == null)
            {
                return Json(new { success = false, message = "An error happened during deleting process" });
            }

            _unitOfWork.Item.Remove(eraserItem);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Item deleted" });
        }
        #region Consume Api
        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data = _unitOfWork.Item.GetAll(includeProperties: "Category") });
        }
        #endregion
    }
}
