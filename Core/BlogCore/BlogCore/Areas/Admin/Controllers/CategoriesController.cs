﻿using BlogCore.Data.Data.Repository;
using BlogCore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoriesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoriesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            // method to load page with the create form
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category category)
        {
            // method to create cateogry
            if (ModelState.IsValid)
            {
                _unitOfWork.Category.Add(category);
                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Category currentCategory = _unitOfWork.Category.Get(id);
            if (currentCategory == null)
            {
                return NotFound();
            }
            return View(currentCategory);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Category category)
        {
            // method to update cateogry
            if (ModelState.IsValid)
            {
                _unitOfWork.Category.UpdateCategory(category);
                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }
        #region Consume Api


        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data = _unitOfWork.Category.GetAll() });
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var categoryToDelete = _unitOfWork.Category.Get(id);
            if (categoryToDelete == null)
            {
                return Json(new { success = false, message = "An error occurred to delete category" });
            }
            _unitOfWork.Category.Remove(categoryToDelete);
            _unitOfWork.Save();
            return Json(new { success = true, message = string.Concat("Category ", categoryToDelete.Name, " was deleted") });
        }

        #endregion
    }
}
