﻿using BlogCore.Data.Data.Repository;
using BlogCore.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BlogCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SlidersController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostingEnviroment;

        public SlidersController(IUnitOfWork unitOfWork, IWebHostEnvironment hostingEnviroment)
        {
            _unitOfWork = unitOfWork;
            _hostingEnviroment = hostingEnviroment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            Slider newSlider = new Slider()
            {
                Name = "",
                UrlImage = ""
            };
            return View(newSlider);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Slider newSlider)
        {
            if (ModelState.IsValid)
            {
                string path = _hostingEnviroment.WebRootPath;
                var files = HttpContext.Request.Form.Files;

                string fileName = Guid.NewGuid().ToString();
                var upload = Path.Combine(path, @"img/items");
                if (files.Count == 0)
                {
                    newSlider.UrlImage = "Select an image";
                    return View(newSlider);
                }
                var extension = Path.GetExtension(files[0].FileName);
                using (var fileStreams = new FileStream(Path.Combine(upload, string.Concat(fileName, extension)), FileMode.Create))
                {
                    files[0].CopyTo(fileStreams);
                }
                newSlider.UrlImage = string.Concat(@"/img/items/", fileName, extension);

                _unitOfWork.Slider.Add(newSlider);
                _unitOfWork.Save();

                return RedirectToAction(nameof(Index));
            }
            return View(newSlider);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id != null)
            {
                var slider = _unitOfWork.Slider.Get(id.GetValueOrDefault());
                return View(slider);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Slider updateSlider)
        {
            if (ModelState.IsValid)
            {
                string sourcePath = _hostingEnviroment.WebRootPath;
                var files = HttpContext.Request.Form.Files;

                var currentSlider = _unitOfWork.Slider.Get(updateSlider.Id);

                if (files.Count > 0)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var upload = Path.Combine(sourcePath, @"img/slider");
                    var extension = Path.GetExtension(files[0].FileName);

                    var pathImage = Path.Combine(sourcePath, currentSlider.UrlImage.TrimStart('/'));
                    if (System.IO.File.Exists(pathImage))
                    {
                        System.IO.File.Delete(pathImage);
                    }
                    using (var fileStreams = new FileStream(Path.Combine(upload, string.Concat(fileName, extension)), FileMode.Create))
                    {
                        files[0].CopyTo(fileStreams);
                    }
                    updateSlider.UrlImage = string.Concat(@"/img/slider/", fileName, extension);

                }
                else
                {
                    updateSlider.UrlImage = currentSlider.UrlImage;
                }
                _unitOfWork.Slider.UpdateSlider(updateSlider);
                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var eraserSlider = _unitOfWork.Slider.Get(id);
            if (eraserSlider == null)
            {
                return Json(new { success = false, message = "An error happened delete the slider" });
            }
            _unitOfWork.Slider.Remove(eraserSlider);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Slider deleted" });
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data = _unitOfWork.Slider.GetAll() });
        }
    }
}
