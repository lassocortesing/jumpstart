﻿using BlogCore.Data.Data.Repository;
using BlogCore.Models;
using System;
using System.Linq;

namespace BlogCore.Data.Data
{
    public class SliderRepository : Repository<Slider>, ISliderRepository
    {
        private readonly ApplicationDbContext _appContext;

        public SliderRepository(ApplicationDbContext appContext) : base(appContext)
        {
            _appContext = appContext;
        }

        public void UpdateSlider(Slider slider)
        {
            var currentSlider = _appContext.Slider.FirstOrDefault(s => s.Id == slider.Id);
            currentSlider.Name = slider.Name;
            currentSlider.State = slider.State;
            currentSlider.UrlImage = slider.UrlImage;
            _appContext.SaveChanges();
        }
    }
}
