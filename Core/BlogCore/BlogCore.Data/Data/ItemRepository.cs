﻿using BlogCore.Data.Data.Repository;
using BlogCore.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlogCore.Data.Data
{
    public class ItemRepository : Repository<Item>, IItemRepository
    {
        private readonly ApplicationDbContext _appContext;

        public ItemRepository(ApplicationDbContext appContext) : base(appContext)
        {
            _appContext = appContext;
        }

        public void UpdateItem(Item item)
        {
            var extractObjSaved = _appContext.Item.FirstOrDefault(s => s.Id == item.Id);
            extractObjSaved.Name = item.Name;
            extractObjSaved.Description = item.Description;
            extractObjSaved.UrlImage = item.UrlImage;
            extractObjSaved.IdCategory = item.IdCategory;
            
        }
    }
}
