﻿
using BlogCore.Data.Data.Repository;
using BlogCore.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlogCore.Data.Data
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        private readonly ApplicationDbContext _appContext;

        public CategoryRepository(ApplicationDbContext appContext) : base(appContext)
        {
            _appContext = appContext;
        }

        public IEnumerable<SelectListItem> GetListCategory()
        {
            return _appContext.Category.Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
        }

        public void UpdateCategory(Category category)
        {
            var extractObjSaved = _appContext.Category.FirstOrDefault(s => s.Id == category.Id);
            extractObjSaved.Name = category.Name;
            extractObjSaved.Order = category.Order;
            _appContext.SaveChanges();
        }
    }
}
