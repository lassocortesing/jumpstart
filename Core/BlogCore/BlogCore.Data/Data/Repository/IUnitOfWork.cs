﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogCore.Data.Data.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoryRepository Category { get; }
        IItemRepository Item { get; }
        ISliderRepository Slider { get; }
        void Save();

    }
}
