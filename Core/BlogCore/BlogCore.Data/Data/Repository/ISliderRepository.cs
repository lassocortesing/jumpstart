﻿using BlogCore.Models;

namespace BlogCore.Data.Data.Repository
{
    public interface ISliderRepository: IRepository<Slider>
    {
        void UpdateSlider(Slider slider);
    }
}
