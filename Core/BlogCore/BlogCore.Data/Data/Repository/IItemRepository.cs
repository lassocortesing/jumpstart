﻿using BlogCore.Models;

namespace BlogCore.Data.Data.Repository
{
    public interface IItemRepository : IRepository<Item>

    {
        void UpdateItem(Item category);
    }
}
