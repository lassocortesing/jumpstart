﻿using BlogCore.Data.Data.Repository;

namespace BlogCore.Data.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _appContext;

        public UnitOfWork(ApplicationDbContext appContext)
        {
            _appContext = appContext;
            Category = new CategoryRepository(_appContext);
            Item = new ItemRepository(_appContext);
            Slider = new SliderRepository(_appContext);
        }

        public ICategoryRepository Category { get; private set; }
        public IItemRepository Item { get; private set; }
        public ISliderRepository Slider { get; private set; }

        public void Dispose()
        {
            _appContext.Dispose();
        }

        public void Save()
        {
            _appContext.SaveChanges();
        }
    }
}
