﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BlogCore.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The name is requiered")]
        [Display(Name = "Category name")]
        public string Name { get; set; }

        [Display(Name = "Display order")]
        public int Order { get; set; }
    }
}
