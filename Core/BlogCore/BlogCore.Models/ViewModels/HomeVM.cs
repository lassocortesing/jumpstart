﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogCore.Models.ViewModels
{
    public class HomeVM
    {
        public IEnumerable<Slider> lstSlider { get; set; }
        public IEnumerable<Item> lstItem { get; set; }
    }
}
