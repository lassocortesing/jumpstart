﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BlogCore.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The name is requiered")]
        [Display(Name = "Name of item")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The description is requiered")]
        public string Description { get; set; }

        [Display(Name = "Creation date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Creation { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(Name = "Image")]
        public string UrlImage { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int IdCategory { get; set; }

        [ForeignKey("IdCategory")]
        public Category Category { get; set; }

    }
}
