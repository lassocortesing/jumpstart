﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BlogCore.Models
{
    public class Slider
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "The name is requiered")]
        [Display(Name = "Name of slider")]
        public string Name { get; set; }

        [Display(Name = "State")]
        public bool State { get; set; }

        
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Image")]
        public string UrlImage { get; set; }
    }
}
