﻿using System;
using System.Collections.Generic;
using System.Text;
using Udemy.Repositories;

namespace Udemy.UnitOfWork
{
    public interface IUnitOfWork
    {
        ICustomerRepository CustomerWork { get; }
    }
}
