﻿using System.Collections.Generic;
using System.Linq;
using Udemy.Models;

namespace Udemy.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        IEnumerable<Customer> CustomerPagedList(int page, int rows);
    }
}
