﻿

using Udemy.Models;
using Udemy.Repositories;
using Udemy.UnitOfWork;

namespace Udemy.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(string connectionString)
        {
            CustomerWork = new CustomerRepository(connectionString);
        }
        

        public ICustomerRepository CustomerWork { get; private set; }
    }
}
