Angular de 0 a experto

Links:{
	Para templates HTML:
	https://www.pixeden.com/html5-website-templates/urku-html5-portfolio-website
	
	Spotify Api:
	https://beta.developer.spotify.com/console/	
}

Comandos:
{ 
	npm install -g @angular/cli
	ng new NewProjectAngular
	ng serve -o
	ng g p  >> crear un pipe
	ng g m >> crear module
	ng g c {el nombre del componente, crea la carpeta con el mismo nombre de forma automatica}
	
	Bootstrap:
	npm install bootstrap --save 
	npm install jquery --save 
	npm install popper.js --save
	npm install font-awesome --save
	
	Instalacion de paquete de configuracion de idioma.
	ng add @angular/localize
	--no se requiere
	
	

	-- Instalar paquete Auth0 de autenticacion
	npm install @auth0/auth0-spa-js --save
	Correo para ingreso al Auth0 de ejemplo:
	correo@test.com
	Test*123
}

Teoría:
{

	TypeScript : {
	
		let: {
			Definir el espacio en memoria donde se va a guardar la informacion de la vairable.
			 Solo existen dentro de el bloque de codigo definido {}.
		}
		
		const: {
				Crear variables para valores que nunca van a cambiar de dato.
				Optimizan la forma de guardar la info (let)
				convencion: Se declaran EN_MAYUSCULAS
		}
		
		Una variable se puede definir con multiples tipos de datos, usando | , ejemplo: let variable : string | number | Date
		En caso de requerir que una vairable llegue de diferete tipo y evitar declararlo como ANY
		
		Interpolado Elegante:{
			`${ VariablString1 } ${ VariablString2 } \n ${ VariablString3 }`;
		}
		
		Parametros en TypeScript:{
			Tip:
			primero  - las variables OBLIGATORIAS
			segundo  - las variables OPCIONALES
			al final - las variables POR DEFECTO
			
			function activar ( obligatoria:string , opcional?: string , porDefecto:string = 'se define esado base')
		}
		
		
		Funciones FLECHA:{
			
			Funcion normal:
				function ( param : string){
				return param;
				}
		
			
			Funcion asignada a una variable:
				const funcionNormal = function ( param : string){
				return param;
				}
				
				
			Funcion flecha:
				const funcionNormal = ( param : string) => {
				return param;
				}
			Caracteristicas:
				No modifican a donde esta apuntando THIS , el contexto incial. 
				Ejemplo:
					En el siguiente ejemplo se genera error en this.nombre porque a perdido la referencia del contexto donde
					this.nombre estaba definida,ahora se apunta al contexto de la funcion anonima autoinvocada y eneste contexto no existe this.nombre:
					
					const hulk = {
						nombre: 'hulk',
						smash () {
							setTimeout ( function () {
								console.log(`${this.nombre} smash!!!!`);
							});
						}
					}
				
					Por lo cual, para que el codigo funcione se debe usar al FLECHA:

					const hulk = {
						nombre: 'hulk',
						smash () {
							setTimeout ( () => {
								console.log(`${this.nombre} smash!!!!`);
							});
						}
					}
		}
		
		
		Objetos y arreglos:{
			
			
			Desestructuracion - Objetos: {
				Para extraer cosas de objetos y arreglos.
				
				 //Definir objeto:
				 const avenger = {
					 nombre : 'Steve',
					 clave  : 'Capitan America',
					 poder : 'Superhumano'
				 }
				 
				 // Desestructurar objeto caso base
				 const {nombre , clave} = avenger;			 
				 
				 extraer (avenger);
				 
				 // Desestructurar el objeto directamente en el argumento de la funcion.
				 function extraer ( {nombre , clave} : any (debe ir el tipo de dato, avenger) ) => {
					 console.log (nombre);
					 console.log (clave);
				 };
			
			}
			
			Desestructuracion - Arreglos : {
				Para extraer cosas de objetos y arreglos.
				//Definir arreglo:
				const avengers : string [] = ['Thor','IronMan','Spiderman']
				
				//Desestructurar arreglo , caso base:
				const [ martillo, , araña] = avengers:
				
				extraerArray (avengers);
				 
				 // Desestructurar el objeto directamente en el argumento de la funcion.
				function extraerArray ( [ martillo, , araña] : string [] ) => {
					 console.log (martillo);
					 console.log (araña);
				 };			
			}		
		}	
		
		
		Promesas:{
			Ejecutar un codigo, sin bloquear la linea principal del codigo de la aplicacion:
			- Usadas para hacer llamados a servicios.
			Ejemplo:
			
			const promesa = new Promise ( (resolver , reject): string => {
				//resolver: lo que se retorna cuando todo funciona OK
				//reject : se retorna cuando se encuentra un error.
				
				setTimeout ( () => {
					resolver ('Termino OK');	
				}, 1000);			
			});
			
			//then para recibir la respuesta OK , el resolver
			//catch para recibir la respuesta fail , el reject
			promesa.then( (mensaje) => console.log (mensaje) )
			.catch( (error) => console.warn (error) ) ;
			
		}
		
		
		Interfaces:{
			Contrato que define un comportamiento.
			Se puede usar para segurarse que un OBJ va cumplir con la condicion.
			
			interface iXmen {
				nombre: string;
				edad: number;
				optionalPower? :string;
			}
			
		}
		
		
		Decoradores: {
			Decorador de clase: Es algo que se coloca antes de la definicion de la clase.
								Permite añadir o ejecutar cosas funcionalidades a una clase.
								El decorador sirve para adicionar las funcionalidades definidas a la clase que use el decorador.
								Angular lo usa para trsnformar las clases en otros objetos, dependiendo de la necesidad
								Funcion que se ejecuta en las clasas para expandirle las funcionalidad.
			Ejemplo:
			function imprimirConsola (funcionCualquiera : Function){
				console.log (funcionCualquiera);
			}
			
			@imprimirConsola
			export class Xmen {
				constructor (
					public nombre:string,
					public clave:string
				){}
				imprimir (){ console.log('imprimir nombre' + this.nombre);}
			}
			
		}

    }

	Angular : {
		
		Componentes:{
			Pequeñas clases que cumplen una funcion específica. Ej: menu de navegacion, barra lateral, pie de pagina...
			Son clases con un decorador especifico.
			
			import {Component} from '@angular/core': Sirve para importar el componente que se utiliza como decorador para que 
			las clases de TypesScript se conviertan en compoenntes
			
			selector: 'app-about': Nos permite definir el nombre con el cual podremos usar el componente en el HTML
			
			Beneficios:
			Permite la re-utilizacion del codigo
			Ayuda a segmentar el codigo, facil mantenimiento
			ayuda a agilizar la generacion de pags o secciones
		}
		
		Directivas estructurales:{
			Son instrucciones que hablan con el HTML y le dictamina que debe hacer
			Ej: ngIf, ngFor
		}
		
		Arranque: {
			- Constructor se ejecuta primero
			- NgOnIniti se ejecuta despues del constructor , y se ejecuta una sola vez
		}
		
		Pipes:{
			-Son para transformar la data de forma visual unicamente
			-No mutan los objeto
			
			async: pipe para leer el resutltado de una promesa u observable
			json: para leer un objeto como cadena ""
			
		}
		
		Oservable: {
			Subscribe: PAra escuchar cualquier data que sea enviada a traves del observable
		}
		
		ciclo de vida:{
			
			1 - Constructor!
			
			ngOnInit: Cuando el componente se esta inicializando ( o despues del 1 ngOnchanges)
			
			ngOnChanges: Cuando la data de las propiedades del componente cambian
			
			ngDoCheck: Cada vez que se hace una revision del  ciclo de deteccion de cambios
			
			ngAfterContentInit: Despues de insertar contenido en la pag (<app-alguna-page>)
			
			ngAfterContentChecked: Despues de la revision del contenido insertado
			
			ngAfterViewInit: Despues de la inicializacion del componente/hijos
			
			ngAfterViewChecked: Despues de  cada revision de los componente/hijos
			
			ngOnDestroy: Justo antes que se destruya el componente o directiva
			
		}		
		
	}

	Style:{
		- Identificar la carpeta de archivos CSS, JS e imagenes o archivos.
		- Ubicar dentro de la carpeta ASSESTS de angular
		- Las referencias del index de los stylos ajustar la refere
		- Al final del index por lo general se encuentran los Scripts
		- # en la URL: Sin el # el server busca una carpeta fisica para extraer el archivos,
			Con el #, los navegadores van a saber que lo que viene despues realmente no es un directorio en el sitio web, 
			es una parte de la ruta del index
	}

	Errores:{
		
		UnauthorizedAccess - creando proyecto : {
			ng : File C:\Users\lasso\AppData\Roaming\npm\ng.ps1 cannot be loaded because running scripts is disabled on this system. For 
			more information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
			At line:1 char:2
			+  ng new Started
			+  ~~
			+ CategoryInfo          : SecurityError: (:) [], PSSecurityException
			+ FullyQualifiedErrorId : UnauthorizedAccess

			Ejecutar comando para solucionar:
			Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
			
		}
	
		Error Token Spotify: {
			El token secret se actualiza cada 1 hora, enotnces se debe actualiar - Ver token en postman
		}
	}
}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	