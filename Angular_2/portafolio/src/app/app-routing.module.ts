import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PortafolioComponent } from './core/portafolio/portafolio.component';
import { AboutComponent } from './core/about/about.component';
import { DetailComponent } from './core/detail/detail.component';

const routes: Routes = [
  { path: 'home', component: PortafolioComponent },
  { path: 'about', component: AboutComponent },
  { path: 'detail', component: DetailComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
