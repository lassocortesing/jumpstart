import {NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FirstComponent } from './components/core/first/first.component';
import { SecondOneComponent } from './components/core/second/second-one/second-one.component';
import { SecondTwoComponent } from './components/core/second/second-two/second-two.component';
import { SecondSearchComponent } from './components/core/second/second-search/second-search.component';

import { StyleDirComponent } from './components/core/styleDirective/style-dir.component';
import { SPOTY_ROUTES } from './components/core/spotify/spoty.routes';
import { WrapperComponent } from './components/core/spotify/wrapper/wrapper.component';
import { AuthGuard } from './components/shared/authentication/service/auth.guard';
import { ProfileComponent } from './components/shared/authentication/profile/profile.component';

const APP_ROUTES: Routes = [
  { path: 'first', component: FirstComponent,canActivate:[AuthGuard] },
  { path: 'secondOne', component: SecondOneComponent },
  { path: 'secondTwo/:id', component: SecondTwoComponent },
  { path: 'search/:item', component: SecondSearchComponent },
  { path: 'wrapper', component: WrapperComponent, children: SPOTY_ROUTES ,canActivate:[AuthGuard]},
  { path: 'style', component: StyleDirComponent ,canActivate:[AuthGuard]},
  { path: 'profile', component: ProfileComponent ,canActivate:[AuthGuard]},
  
  { path: '**', pathMatch: 'full', redirectTo: 'secondOne' },
];

@NgModule({
  imports:[RouterModule.forRoot(APP_ROUTES)],
  exports:[RouterModule]
})
export class app_routing {};
