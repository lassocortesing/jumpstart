export interface iHeroe {
  id:string;
  name: string;
  bio: string;
  img: string;
  appear: string;
  home: string;
}
