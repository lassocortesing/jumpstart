import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
//Se requiere FormsModule para conectar html con ts en dos direcciones
import { FormsModule } from '@angular/forms';
import loacalEn from '@angular/common/locales/en';
import { HttpClientModule } from '@angular/common/http';

registerLocaleData(loacalEn);
// Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { FirstComponent } from './components/core/first/first.component';
import { SecondOneComponent } from './components/core/second/second-one/second-one.component';
import { SecondTwoComponent } from './components/core/second/second-two/second-two.component';
import { SecondSearchComponent } from './components/core/second/second-search/second-search.component';
import { CardComponent } from './components/shared/card/card.component';
import { HomeComponent } from './components/core/spotify/home/home.component';
import { SearchComponent } from './components/core/spotify/search/search.component';
import { ArtistComponent } from './components/core/spotify/artist/artist.component';
// Routs
import { app_routing } from './app.routes';
// Services
import { HeroesService } from './components/core/second/service/heroes.service';
import { registerLocaleData } from '@angular/common';
import { SpotifyService } from './components/core/spotify/service/spotify.service';
import { SpotiCardComponent } from './components/shared/spoti-card/spoti-card.component';
//Pipes
import { ValidateImagePipe } from './components/shared/pipe/validate-image.pipe';
import { MyPipePipe } from './components/shared/pipe/my-pipe.pipe';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { IdArtistPipe } from './components/shared/pipe/id-artist.pipe';
import { DivErrorComponent } from './components/shared/div-error/div-error.component';
import { StyleDirComponent } from './components/core/styleDirective/style-dir.component';
import { HighlightDirective } from './components/shared/directive/highlight.directive';
import { WrapperComponent } from './components/core/spotify/wrapper/wrapper.component';
import { ProfileComponent } from './components/shared/authentication/profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FirstComponent,
    SecondOneComponent,
    SecondTwoComponent,
    SecondSearchComponent,
    CardComponent,
    MyPipePipe,
    HomeComponent,
    SearchComponent,
    ArtistComponent,
    SpotiCardComponent,
    ValidateImagePipe,
    LoadingComponent,
    IdArtistPipe,
    DivErrorComponent,
    StyleDirComponent,
    HighlightDirective,
    WrapperComponent,
    ProfileComponent,
  ],
  imports: [BrowserModule, HttpClientModule, FormsModule, app_routing],
  providers: [
    HeroesService,
    SpotifyService,
    { provide: LOCALE_ID, useValue: 'en' },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
