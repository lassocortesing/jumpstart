import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css'],
})
export class FirstComponent implements OnInit {
  public isEnable: boolean = true;
  public listData: string[];

  constructor() {
    this.listData=['Spiderman','Venom','Dr Octopus','Duende verde']
  }

  ngOnInit(): void {}

  actionButton() {
    this.isEnable = !this.isEnable;
  }
}
