import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondSearchComponent } from './second-search.component';

describe('SecondSearchComponent', () => {
  let component: SecondSearchComponent;
  let fixture: ComponentFixture<SecondSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
