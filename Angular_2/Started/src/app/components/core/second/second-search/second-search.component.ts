import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { iHeroe } from 'src/app/model/iHeroe.model';
import { HeroesService } from 'src/app/components/core/second/service/heroes.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-second-search',
  templateUrl: './second-search.component.html',
  styleUrls: ['./second-search.component.css'],
})
export class SecondSearchComponent implements OnInit {
  searchHero: iHeroe[] = [];
  hadData: boolean = true;
  constructor(
    private activateRoute: ActivatedRoute,
    private heroService: HeroesService,
    private route: Router
  ) {
    this.activateRoute.params.subscribe((urlParam) => {
      this.searchHero = this.heroService.searchHeroe(urlParam['item']);
      if (!this.searchHero) {
        this.hadData = false;
      } else if (this.searchHero.length == 0) {
        this.hadData = false;
      } else {
        this.hadData = true;
      }
    });
  }

  ngOnInit(): void {}

  reciveOutPut(paramOut: string) {
    this.route.navigate(['/secondTwo', paramOut]);
  }
}
