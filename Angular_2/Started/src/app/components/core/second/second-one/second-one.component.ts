import { Component, OnInit } from '@angular/core';
import { HeroesService } from 'src/app/components/core/second/service/heroes.service';
import { iHeroe } from 'src/app/model/iHeroe.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-second-one',
  templateUrl: './second-one.component.html',
  styleUrls: ['./second-one.component.css'],
})
export class SecondOneComponent implements OnInit {
  public listHeroes: iHeroe[] = [];
  constructor(private _heroeService: HeroesService, private route: Router) {}

  ngOnInit(): void {
    this.listHeroes = this._heroeService.getHeroes();
  }

  reciveOutPut(paramOut: string) {
    this.route.navigate(['/secondTwo', paramOut]);
  }
}
