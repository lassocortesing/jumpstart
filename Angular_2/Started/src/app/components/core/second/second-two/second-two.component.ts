import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { iHeroe } from 'src/app/model/iHeroe.model';
import { HeroesService } from 'src/app/components/core/second/service/heroes.service';

@Component({
  selector: 'app-second-two',
  templateUrl: './second-two.component.html',
  styleUrls: ['./second-two.component.css'],
})
export class SecondTwoComponent implements OnInit {
  public hereoDetail: iHeroe;
  constructor(
    private activateRouter: ActivatedRoute,
    private _heroeService: HeroesService
  ) {
    this.activateRouter.params.subscribe((params) => {
      this.hereoDetail = this._heroeService.getHeroe(params['id']);
    });
  }

  ngOnInit(): void {}
}
