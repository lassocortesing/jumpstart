import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../service/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  public artists: any[] = [];
  public loading:boolean;
  public error: boolean = false;
  public errorMessage:string='';
  constructor(private spotifyService: SpotifyService) {
    this.loading=false;
  }

  ngOnInit(): void {}
  search(text: string) {
    this.loading=true;
    this.spotifyService.getArtists(text).subscribe((result) => {
      this.artists = result;
      this.loading=false;
    },
    (fail) => {
      this.error = true;
      this.loading=false;
      this.errorMessage = fail.error.error.message;
    });
  }
}
