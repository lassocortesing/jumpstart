import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ArtistComponent } from './artist/artist.component';
import { SearchComponent } from './search/search.component';

export const SPOTY_ROUTES: Routes = [
  { path: 'spotyApp', component: HomeComponent },
  { path: 'artistApp/:id', component: ArtistComponent },
  { path: 'searchApp', component: SearchComponent },

  { path: '**', pathMatch: 'full', redirectTo: 'spotyApp' },
];


