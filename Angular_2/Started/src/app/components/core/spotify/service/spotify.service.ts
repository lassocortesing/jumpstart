import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class SpotifyService {
  constructor(private http: HttpClient) {}

  generate
  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      Authorization:
        'Bearer BQAkS1eFK6sr2ZEQ2rxjOb_cN9S0fy7m2L8QitzfYbbnfA2RZeWZDXWxGXYDX05PjJTdgtSmWvjwzBkTTqM',
    });

    return this.http.get(url, { headers });
  }

  getNewReleases(): Observable<any> {
    return this.getQuery(`browse/new-releases`).pipe(
      map((resultRow) => {
        return resultRow['albums'].items;
      })
    );
  }

  getArtists(artist: string): Observable<any> {
    return this.getQuery(
      `search?query=${artist}&type=artist&offset=0&limit=15`
    ).pipe(
      map((result) => {
        return result['artists'].items;
      })
    );
  }

  getArtist(idArtis: string):Observable<any> {
    return this.getQuery(`artists/${idArtis}`).pipe(
      map((result) => {
        return result;
      })
    );
  }

  getTopSoundTrack(idArtis: string):Observable<any> {
    return this.getQuery(`artists/${idArtis}/top-tracks?country=us`).pipe(
      map((result) => {
        
        return result['tracks'];
      })
    );
  }
}
