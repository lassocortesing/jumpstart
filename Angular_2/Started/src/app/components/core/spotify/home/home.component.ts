import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../service/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public newSongs: any[] = [];
  public loading: boolean;
  public error: boolean = false;
  public errorMessage:string='';
  constructor(private spotifyService: SpotifyService) {
    this.loading = true;
    this.spotifyService.getNewReleases().subscribe(
      (result) => {
        this.newSongs = result;
        this.loading = false;
      },
      (fail) => {
        this.error = true;
        this.loading=false;
        this.errorMessage = fail.error.error.message;
      }
    );
  }

  ngOnInit(): void {}
}
