import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../service/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css'],
})
export class ArtistComponent implements OnInit {
  public artist: any = {};
  public loading: boolean = true;
  public songs: any[] = [];
  constructor(
    private activeRoute: ActivatedRoute,
    private spotiService: SpotifyService
  ) {
    this.loading = true;
    this.activeRoute.params.subscribe((param) => {
      this.getArtist(param['id']);
      this.getSoundTrack(param['id']);
    });
  }

  ngOnInit(): void {}

  getArtist(idArtist: string) {
    this.spotiService.getArtist(idArtist).subscribe((result) => {
      this.artist = result;
      this.loading = false;
    });
  }

  getSoundTrack(idArtist: string) {
    this.spotiService.getTopSoundTrack(idArtist).subscribe((result) => {
      this.songs = result;
      this.loading = false;
    });
  }
}
