import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleDirComponent } from './style-dir.component';

describe('StyleDirComponent', () => {
  let component: StyleDirComponent;
  let fixture: ComponentFixture<StyleDirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleDirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleDirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
