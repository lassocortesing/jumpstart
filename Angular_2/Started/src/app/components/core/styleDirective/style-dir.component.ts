import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-style-dir',
  templateUrl: './style-dir.component.html',
  styleUrls: ['./style-dir.component.css'],
})
export class StyleDirComponent implements OnInit {
  public sizeLetter: number = 15;
  public listAlert: string[] = [
    'alert-success',
    'alert-danger',
    'alert-warning',
    'alert-info',
  ];
  public styleAlert: string = this.listAlert[0];

  public loading: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  changeSize(operation: string) {
    switch (operation) {
      case '+':
        this.sizeLetter += 5;

        break;
      case '-':
        this.sizeLetter -= 5;

        break;
      default:
        break;
    }
  }

  changeAlert(operation: string) {
    switch (operation) {
      case '0':
        this.styleAlert = this.listAlert[operation];
        break;
      case '1':
        this.styleAlert = this.listAlert[operation];
        break;
      case '2':
        this.styleAlert = this.listAlert[operation];
        break;
      case '3':
        this.styleAlert = this.listAlert[operation];
        break;
      default:
        break;
    }
  }

  ejectProcess() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }
}
