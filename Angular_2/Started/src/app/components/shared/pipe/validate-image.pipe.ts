import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'validateImage',
})
export class ValidateImagePipe implements PipeTransform {
  transform(sourceImage: any[]): string {
    if (!sourceImage) {
      return 'assets/img/no-image.jpg';
    }
    if (sourceImage.length > 0) {
      return sourceImage[0].url;
    } else {
      return 'assets/img/no-image.jpg';
    }
  }
}
