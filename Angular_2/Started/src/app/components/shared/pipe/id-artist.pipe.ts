import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'idArtist',
})
export class IdArtistPipe implements PipeTransform {
  transform(item: any): string {
  
    if (item.type === 'artist' || item.type === 'single') {
      return item.id;
    } else {
      return item.artists[0].id;
    }
  }
}
