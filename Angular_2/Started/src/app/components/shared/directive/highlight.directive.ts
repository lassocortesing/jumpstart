import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]',
})
export class HighlightDirective {
  @Input('colorExt') color: string;
  constructor(private er: ElementRef) {}

  @HostListener('mouseenter') mouseIn() {
    this.resaltar(this.color || 'blue');
  }

  @HostListener('mouseout') mouseOut() {
    this.resaltar('red');
  }

  private resaltar(color: string) {
    this.er.nativeElement.style.backgroundColor = color;
  }
}
