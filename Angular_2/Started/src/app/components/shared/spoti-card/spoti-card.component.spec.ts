import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotiCardComponent } from './spoti-card.component';

describe('SpotiCardComponent', () => {
  let component: SpotiCardComponent;
  let fixture: ComponentFixture<SpotiCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpotiCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotiCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
