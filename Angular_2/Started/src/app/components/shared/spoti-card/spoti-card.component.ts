import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-spoti-card',
  templateUrl: './spoti-card.component.html',
  styleUrls: ['./spoti-card.component.css'],
})
export class SpotiCardComponent implements OnInit {
  @Input() titleCard: string = '';
  @Input() imgCard: string = '';
  @Input() bodyCard: any[] = [];
  @Input() idArtist: string;

  constructor(private route: Router) {}

  ngOnInit(): void {}

  viewArtist() {
    this.route.navigate(['/wrapper/artistApp',this.idArtist]);

  }
}
