import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { iProfile } from 'src/app/model/iProfile.model';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  public profileLogued: iProfile={};
  constructor(public auth: AuthService) {
    auth.userProfile$.pipe(filter((x) => x != null)).subscribe((result) => {
      this.profileLogued = result;
    });
  }

  ngOnInit(): void {}

  eventoBtn():void{
    console.log("Form", this.profileLogued);
  }
}
