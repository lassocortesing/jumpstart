import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../authentication/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.html',
})
export class HeaderComponent {
  constructor(private route: Router, public auth:AuthService) {}
 
  buscarNavBar(item: string) {
    this.route.navigate(['/search',item]);
  }
}
