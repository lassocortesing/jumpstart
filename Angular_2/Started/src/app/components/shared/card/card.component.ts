import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() titleCard: string = '';
  @Input() bioCard: string = '';
  @Input() appearCard: string = '';
  @Input() imgCard: string = '';
  @Input() i: string = '';

  @Output() showDetailCard: EventEmitter<string>;

  constructor() {
    this.showDetailCard = new EventEmitter();
  }

  ngOnInit(): void {
    
  }

  showDetail() {
    this.showDetailCard.emit(this.i);
  }
}
