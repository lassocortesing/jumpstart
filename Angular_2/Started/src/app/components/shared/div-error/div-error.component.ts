import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-div-error',
  templateUrl: './div-error.component.html',
  styleUrls: ['./div-error.component.css']
})
export class DivErrorComponent implements OnInit {

  @Input() message : string ='';
  constructor() { }

  ngOnInit(): void {
  }

}
