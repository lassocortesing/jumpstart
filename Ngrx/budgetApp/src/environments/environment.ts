// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAKOEnluXy3q9NzSMeV5qL1WGD7bo4aNVo",
    authDomain: "budgetapp-a96e9.firebaseapp.com",
    databaseURL: "https://budgetapp-a96e9.firebaseio.com",
    projectId: "budgetapp-a96e9",
    storageBucket: "budgetapp-a96e9.appspot.com",
    messagingSenderId: "744948571231",
    appId: "1:744948571231:web:7798c8ec8eec5621f34fea"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
