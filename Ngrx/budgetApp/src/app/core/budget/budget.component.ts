import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { budgetModel } from 'src/app/model/budget.model';
import { BudgetService } from '../service/budget.service';
import Swal from 'sweetalert2';
import { budgetState } from 'src/app/shared/ngrx/states/budget.state';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { isLoading, stopLoading } from 'src/app/shared/ngrx/actions/ui.actions';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css'],
})
export class BudgetComponent implements OnInit, OnDestroy {
  budgetForm: FormGroup;
  esIngreso: boolean = true;
  loadingSubs: Subscription;
  loading: boolean = false;
  constructor(
    private fb: FormBuilder,
    private fireStore: BudgetService,
    private store: Store<budgetState>
  ) {}
  ngOnDestroy(): void {
    this.loadingSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.budgetForm = this.fb.group({
      descriptionFr: ['', Validators.required],
      amountFr: ['', Validators.required],
    });

    this.loadingSubs = this.store
      .select('uiAction')      
      .subscribe((result) => {
        this.loading = result.loadingState;
      });
  }

  saveBudget() {
    if (this.budgetForm.valid) {
      this.store.dispatch(isLoading());
      const myBudget: budgetModel = {
        description: this.budgetForm.value['descriptionFr'],
        amount: this.budgetForm.value['amountFr'],
        type: this.esIngreso ? 'ingreso' : 'egreso',
      };
      this.fireStore
        .createInputBudget(myBudget)
        .then((result) => {
          this.budgetForm.reset();
          this.store.dispatch(stopLoading());
          Swal.close();
          Swal.fire('Item registred :', myBudget.description, 'success');
        })
        .catch((fail) => {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: fail.message,
            footer: fail.code,
          });
          this.store.dispatch(stopLoading());
        });
    }
  }
  typeAmount() {
    this.esIngreso = !this.esIngreso;
  }
}
