import { Component, OnInit, OnDestroy } from '@angular/core';
import { budgetState } from 'src/app/shared/ngrx/states/budget.state';
import { Store } from '@ngrx/store';
import { BudgetService } from '../../service/budget.service';
import { budgetModel } from 'src/app/model/budget.model';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { userModel } from 'src/app/model/user.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
})
export class DetailComponent implements OnInit, OnDestroy {
  budgetData: budgetModel[] = [];
  budSubs: Subscription;
  userSubs: Subscription;
  constructor(
    private store: Store<budgetState>,
    private budgetService: BudgetService
  ) {}
  ngOnDestroy(): void {
    this.budSubs.unsubscribe();

    if (this.userSubs) this.userSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.budSubs = this.store
      .select('itemBudgetAction')
      .pipe(
        filter((resultFl) => {
          return (
            resultFl &&
            resultFl != null &&
            resultFl.itemsState != undefined &&
            resultFl.itemsState.length != 0
          );
        })
      )
      .subscribe((result) => {
        this.budgetData = result.itemsState;
      });
  }

  eraseItem(id: string) {
    this.userSubs = this.store
      .select('authAction')
      .pipe(
        filter((resultFilter) => {
          debugger;
          return (
            resultFilter &&
            resultFilter.userSt &&
            resultFilter.userSt != null &&
            resultFilter.userSt.email != ''
          );
        }),
        map((resultMap) => {
          return resultMap.userSt;
        })
      )
      .subscribe((resultUser: userModel) => {
        this.budgetService
          .deleteItemBudget(resultUser, id)
          .then(() => {
            Swal.fire('Erased', 'Item erased', 'success');
          })
          .catch((fail) => {
            Swal.fire('Erased', fail.message, 'error');
          });
      });
  }
}
