import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { RouterModule } from '@angular/router';
//Our componentes
import { StatisticsComponent } from './statistics/statistics.component';
import { DetailComponent } from './detail/detail.component';
import { DashBoardComponent } from '../dash-board/dash-board.component';
import { BudgetComponent } from './budget.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashModule } from '../dash-board/dash.module';

@NgModule({
  declarations: [
    DashBoardComponent,
    BudgetComponent,
    StatisticsComponent,
    DetailComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    RouterModule,
  ],
  exports: [
    DashBoardComponent,
    BudgetComponent,
    StatisticsComponent,
    DetailComponent,
    DashModule,
  ],
})
export class BudgetModule {}
