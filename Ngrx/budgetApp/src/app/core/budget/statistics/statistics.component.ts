import { Component, OnInit, OnDestroy } from '@angular/core';
import { budgetState } from 'src/app/shared/ngrx/states/budget.state';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { budgetModel } from 'src/app/model/budget.model';
import { Subscription } from 'rxjs';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css'],
})
export class StatisticsComponent implements OnInit, OnDestroy {
  ingresos: number = 0;
  egresos: number = 0;
  totalIn: number = 0;
  totalEg: number = 0;
  itemsSub: Subscription;

  public lblChart: Label[] = ['Ingresos', 'Egresos'];
  public dataChart: MultiDataSet = [[]];
  public typeChart: ChartType = 'doughnut';

  constructor(private store: Store<budgetState>) {}
  ngOnDestroy(): void {
    this.itemsSub.unsubscribe();
  }

  ngOnInit(): void {
    this.itemsSub = this.store
      .select('itemBudgetAction')
      .pipe(
        filter((resultFl) => {
          return (
            resultFl &&
            resultFl != undefined &&
            resultFl.itemsState != null &&
            resultFl.itemsState.length != 0
          );
        })
      )
      .subscribe((result) => {
        this.generateStatistics(result.itemsState);
      });
  }

  generateStatistics(listItems: budgetModel[]) {
    this.totalIn = 0;
    this.totalEg = 0;
    this.ingresos = 0;
    this.egresos = 0;
    for (const item of listItems) {
      if (item.type === 'ingreso') {
        this.totalIn += item.amount;
        this.ingresos++;
      } else {
        this.totalEg += item.amount;
        this.egresos++;
      }
    }
    this.dataChart = [[this.totalIn, this.totalEg]];
  }
}
