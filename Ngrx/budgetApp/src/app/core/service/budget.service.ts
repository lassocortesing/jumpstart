import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { budgetModel } from 'src/app/model/budget.model';
import { AuthService } from 'src/app/shared/auth/service/auth.service';
import { userModel } from 'src/app/model/user.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BudgetService {
  constructor(private fireStore: AngularFirestore, private auth: AuthService) {}

  createInputBudget(input: budgetModel) {
    return this.fireStore
      .doc(`${this.auth.getUser().id}/budget`)
      .collection('items')
      .add({ ...input });
  }

  getItemsBudget(uid: userModel): Observable<budgetModel[]> {
    return this.fireStore
      .collection(`${uid.id}/budget/items`)
      .snapshotChanges()
      .pipe(
        map((resultSnap) => {
          return resultSnap.map((snap) => {
            return {
              id: snap.payload.doc.id,
              ...(snap.payload.doc.data() as any),
            };
          });
        })
      );
  }

  deleteItemBudget(uid: userModel, idItem: string) {
    return this.fireStore.doc(`${uid.id}/budget/items/${idItem}`).delete();
  }
}
