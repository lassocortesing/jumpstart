import { Routes } from '@angular/router';
import { StatisticsComponent } from '../budget/statistics/statistics.component';
import { BudgetComponent } from '../budget/budget.component';
import { DetailComponent } from '../budget/detail/detail.component';

export const dashBoardRoutes: Routes = [
  { path: '', component: StatisticsComponent },
  { path: 'budget', component: BudgetComponent },
  { path: 'detail', component: DetailComponent },
];
