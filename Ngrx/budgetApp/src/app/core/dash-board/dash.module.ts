import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashBoardComponent } from './dash-board.component';
import { dashBoardRoutes } from './dashBoard.routes';

const routesChildre: Routes = [
  {
    path: '',
    component: DashBoardComponent,
    children: dashBoardRoutes,
    // canActivate: [AuthGuard],
  },
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routesChildre)],
  exports: [RouterModule],
})
export class DashModule {}
