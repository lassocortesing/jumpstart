import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { budgetState } from 'src/app/shared/ngrx/states/budget.state';
import { filter, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { userModel } from 'src/app/model/user.model';
import { BudgetService } from '../service/budget.service';
import { budgetModel } from 'src/app/model/budget.model';
import { setItem } from 'src/app/shared/ngrx/actions/itemsBudget.actions';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.css'],
})
export class DashBoardComponent implements OnInit, OnDestroy {
  userSubs: Subscription;
  budSubs: Subscription;
  constructor(
    private store: Store<budgetState>,
    private budgetService: BudgetService
  ) {}
  ngOnDestroy(): void {
    this.userSubs.unsubscribe();
    this.budSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.userSubs = this.store
      .select('authAction')
      .pipe(
        filter((resultFilter) => {
          return (
            resultFilter && resultFilter.userSt && resultFilter.userSt != null
          );
        }),
        map((resultMap) => {
          return resultMap.userSt;
        })
      )
      .subscribe((resultUser: userModel) => {
        this.budSubs = this.budgetService
          .getItemsBudget(resultUser)
          .subscribe((result) => {
            this.store.dispatch(setItem({ payloadBudget: result }));
          });
      });
  }
}
