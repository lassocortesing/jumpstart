export interface userModel {
  id?: string;
  email: string;
  password: string;
  isLogin?: boolean;
  name?: string;
}
