export interface budgetModel {
  description: string;
  amount: number;
  type: string;
  id?: string;
}
