import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//FireBase - FireStore
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
//NGRX
import { StoreModule } from '@ngrx/store';
import { budgetReducer } from './shared/ngrx/reducers/budget.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
//Default
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';

//Modulos:
import { AuthModule } from './shared/auth/auth.module';
import { SharedModule } from './shared/shared.module';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AuthModule,
    SharedModule,    
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    StoreModule.forRoot(budgetReducer),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
