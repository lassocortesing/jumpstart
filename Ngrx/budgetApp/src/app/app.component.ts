import { Component } from '@angular/core';
import { AuthService } from './shared/auth/service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'budgetApp';

  constructor(private auth:AuthService){
    this.auth.getStateLogin();
  }
}
