import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared/auth/login/login.component';
import { RegisterComponent } from './shared/auth/register/register.component';
import { DashBoardComponent } from './core/dash-board/dash-board.component';
import { dashBoardRoutes } from './core/dash-board/dashBoard.routes';
import { AuthGuard } from './shared/auth/service/auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: '',
    canLoad: [AuthGuard],
    loadChildren: () =>
      import('./core/budget/budget.module').then((m) => {
        return m.BudgetModule;
      }),
  },
  { path: '**', redirectTo: '' },
  // { path: '', component: DashBoardComponent, children: dashBoardRoutes ,canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
