import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//Our componentes
import { FooterComponent } from './footer/footer.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { OrderItemsPipe } from './pipes/order-items.pipe';
import { HeaderComponent } from './header/header.component';
@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    SideBarComponent,
    OrderItemsPipe,
  ],
  imports: [CommonModule, RouterModule],
  exports: [FooterComponent, HeaderComponent, SideBarComponent, OrderItemsPipe],
})
export class SharedModule {}
