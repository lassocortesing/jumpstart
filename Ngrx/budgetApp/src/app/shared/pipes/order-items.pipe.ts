import { Pipe, PipeTransform } from '@angular/core';
import { budgetModel } from 'src/app/model/budget.model';

@Pipe({
  name: 'orderItems',
})
export class OrderItemsPipe implements PipeTransform {
  transform(value: budgetModel[]): budgetModel[] {
    if (value.length != 0) {
      value = value.slice().sort((a, b) => {
        if (a.type === 'ingreso') {
          return -1;
        } else {
          return 1;
        }
      });
    }
    return value;
  }
}
