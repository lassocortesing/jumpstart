import { ActionReducerMap } from '@ngrx/store';
import { budgetState } from '../states/budget.state';
import { uiReducer } from './ui.reducer';
import { authReducer } from './auth.reducer';
import { itemsBudgetReducer } from './itemsBudget.reducer';

export const budgetReducer: ActionReducerMap<budgetState> = {
  uiAction: uiReducer,
  authAction: authReducer,
  itemBudgetAction: itemsBudgetReducer,
};
