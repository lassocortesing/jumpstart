import { Action } from '@ngrx/store';
import { authState } from '../states/auth.state';
import { setUser, unSetUser } from '../actions/auth.actions';

const initialState: authState = null;

export function authReducer(state: authState = initialState, action: Action) {
  switch (action.type) {
    case setUser.type:
      return { ...state, userSt: { ...action['payloadUser'] } };
    case unSetUser.type:
      return { ...state, userSt: null };
    default:
      return state;
  }
}
