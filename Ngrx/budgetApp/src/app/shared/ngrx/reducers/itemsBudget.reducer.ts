import { Action } from '@ngrx/store';
import { setItem, unSetItem } from '../actions/itemsBudget.actions';
import { itemsBudgetState } from '../states/itemsBudget.state';

const initialState: itemsBudgetState = { itemsState: [] };

export function itemsBudgetReducer(
  state: itemsBudgetState = initialState,
  action: Action
) {
  switch (action.type) {
    case setItem.type:
      return { ...state, itemsState: [...action['payloadBudget']] };
    case unSetItem.type:
      return { ...state, itemsState: [] };
    default:
      return state;
  }
}
