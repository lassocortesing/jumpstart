import { Action } from '@ngrx/store';
import { uiState } from '../states/ui.state';
import { isLoading, stopLoading } from '../actions/ui.actions';

const initialState: uiState = { loadingState: false };

export function uiReducer(state: uiState = initialState, action: Action) {
  switch (action.type) {
    case isLoading.type:
      let a: string = '';
      return { ...state, loadingState: true };
    case stopLoading.type:
      let b: string = '';
      return { ...state, loadingState: false };

    default:
      return state;
  }
}
