import { uiState } from './ui.state';
import { authState } from './auth.state';
import { itemsBudgetState } from './itemsBudget.state';

export interface budgetState {
  uiAction: uiState;
  authAction: authState;
  itemBudgetAction: itemsBudgetState;
}
