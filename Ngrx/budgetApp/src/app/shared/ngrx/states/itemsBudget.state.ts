import { budgetModel } from 'src/app/model/budget.model';

export interface itemsBudgetState {
  itemsState: budgetModel[];
}
