import { createAction, props } from '@ngrx/store';
import { userModel } from 'src/app/model/user.model';

export const setUser = createAction(
  '[AuthState] setUser',
  props<{ payloadUser: userModel }>()
);
export const unSetUser = createAction('[AuthState] unSetUser');
