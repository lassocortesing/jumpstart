import { createAction, props } from '@ngrx/store';
import { budgetModel } from 'src/app/model/budget.model';

export const setItem = createAction(
  '[Budget] setItem',
  props<{ payloadBudget: budgetModel[] }>()
);
export const unSetItem = createAction('[Budget] unSetItem');
