import { createAction, props } from '@ngrx/store';

export const isLoading = createAction('[loadingState] isLoading');
export const stopLoading = createAction('[loadingState] stopLoading');