import { Observable, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { userModel } from 'src/app/model/user.model';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { budgetState } from '../../ngrx/states/budget.state';
import { Store } from '@ngrx/store';
import { setUser, unSetUser } from '../../ngrx/actions/auth.actions';
import { unSetItem } from '../../ngrx/actions/itemsBudget.actions';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userSubs: Subscription;
  private _user: userModel;
  constructor(
    public fireBase: AngularFireAuth,
    private fireStore: AngularFirestore,
    private store: Store<budgetState>
  ) {}

  getStateLogin() {
    this.fireBase.authState.subscribe((result) => {
      
      if (result) {
        this.userSubs = this.fireStore
          .doc(`${result.uid}/usuario`)
          .valueChanges()
          .subscribe((result: userModel) => {
            this._user = result;
            this.store.dispatch(setUser({ payloadUser: result }));
          });
      } else {
        this._user = null;
        this.store.dispatch(unSetItem());
        if (this.userSubs) {
          this.userSubs.unsubscribe();
        }
        this.store.dispatch(unSetUser());
      }
    });
  }

  getUser(): userModel {
    return { ...this._user };
  }
  isAuth(): Observable<boolean> {
    return this.fireBase.authState.pipe(
      map((x) => {
        return x != null;
      })
    );
  }

  createUser(user: userModel) {
    return this.fireBase
      .createUserWithEmailAndPassword(user.email, user.password)
      .then((result) => {
        user.id = result.user.uid;
        user.isLogin = true;
        return this.fireStore.doc(`${user.id}/usuario`).set({ ...user });
      });
  }

  login(user: userModel) {
    return this.fireBase.signInWithEmailAndPassword(user.email, user.password);
  }

  logOut() {
    this.fireBase.signOut();
  }
}
