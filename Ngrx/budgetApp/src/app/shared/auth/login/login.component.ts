import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule,
} from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { userModel } from 'src/app/model/user.model';
import Swal from 'sweetalert2';
import { budgetState } from '../../ngrx/states/budget.state';
import { Store } from '@ngrx/store';
import { isLoading, stopLoading } from '../../ngrx/actions/ui.actions';
import { Subscribable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginFormGroup: FormGroup;
  loading: boolean = false;
  uiSubs: Subscription;
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private store: Store<budgetState>
  ) {}

  ngOnDestroy(): void {
    this.uiSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.loginFormGroup = this.fb.group({
      correoFr: ['', [Validators.required, Validators.email]],
      contrasenaFr: ['', Validators.required],
    });

    this.uiSubs = this.store.select('uiAction').subscribe((result) => {
      this.loading = result.loadingState;
    });
  }

  logIn() {
    this.store.dispatch(isLoading());
    Swal.fire({
      title: '... Loading ...',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    if (this.loginFormGroup.valid) {
      let user: userModel = {
        name: this.loginFormGroup.value['nombreFr'],
        email: this.loginFormGroup.value['correoFr'],
        password: this.loginFormGroup.value['contrasenaFr'],
        isLogin: false,
      };
      this.auth
        .login(user)
        .then((result) => {
          user.id = result.user.uid;
          Swal.close();
          this.store.dispatch(stopLoading());
          this.router.navigate(['/']);
        })
        .catch((fail) => {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: fail.message,
            footer: fail.code,
          });
          this.store.dispatch(stopLoading());
        });
    }
  }
}
