import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ReactiveFormsModule,
  FormControl,
} from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { userModel } from 'src/app/model/user.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Subscription } from 'rxjs';
import { budgetState } from '../../ngrx/states/budget.state';
import { Store } from '@ngrx/store';
import { isLoading, stopLoading } from '../../ngrx/actions/ui.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  public registerFormGroup: FormGroup;
  uiSubs: Subscription;
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private store: Store<budgetState>
  ) {}
  ngOnDestroy(): void {
    this.uiSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.registerFormGroup = this.fb.group({
      nombreFr: ['', Validators.required],
      correoFr: ['', [Validators.required, Validators.email]],
      contrasenaFr: ['', Validators.required],
    });

    this.uiSubs = this.store.select('uiAction').subscribe((result) => {
      this.loading = result.loadingState;
    });
  }

  createUser() {
    this.store.dispatch(isLoading());
    
    Swal.fire({
      title: '... Loading ...',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });

    if (this.registerFormGroup.valid) {
      let user: userModel = {
        name: this.registerFormGroup.value['nombreFr'],
        email: this.registerFormGroup.value['correoFr'],
        password: this.registerFormGroup.value['contrasenaFr'],
        isLogin: false,
      };
      this.auth
        .createUser(user)
        .then((result) => {
          Swal.close();
          this.store.dispatch(stopLoading());
          this.router.navigate(['/']);
        })
        .catch((fail) => {
          
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: fail.message,
            footer: fail.code,
          });
          this.store.dispatch(stopLoading());
        });

    }
  }
}
