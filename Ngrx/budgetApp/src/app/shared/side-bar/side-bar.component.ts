import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';
import { Router } from '@angular/router';
import { budgetState } from '../ngrx/states/budget.state';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { userModel } from 'src/app/model/user.model';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css'],
})
export class SideBarComponent implements OnInit {
  public user: userModel = { name: '', email: '', password: '' };
  constructor(
    private auth: AuthService,
    private router: Router,
    private store: Store<budgetState>
  ) {}

  ngOnInit(): void {
    this.store
      .select('authAction')
      .pipe(
        filter((rf) => {
          return rf && rf.userSt && rf.userSt != null;
        })
      )
      .subscribe((result) => {
        this.user = result.userSt;
      });
  }

  logOut() {
    this.auth.logOut();
    this.router.navigate(['/login']);
  }
}
