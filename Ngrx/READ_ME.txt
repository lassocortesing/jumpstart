NGRX de 0 a experto

https://es.redux.js.org
	Bootstrap:
	npm install bootstrap --save 
	npm install jquery --save 
	npm install popper.js --save
	
	npm install @ngrx/store --save
	npm install @ngrx/store-devtools --save
	npm install @ngrx/effects --save
	
	Angular Fire para trabajr con FireBase de google
	ng add @angular/fire
	npm i firebase
	
	PopUps:
	npm install sweetalert2
	
	Guard:
	Para aplicar canLoad o canAccept y validar si se puede o no cargar el componente web
	npm g guard auth
	
	Graficas de JS:
	npm install --save ng2-charts
	npm install --save chart.js
	
	4/ywHwm7tTYIH9F7TGrjc2_8O2zQQizMOLuI2oM9705KJgrgisE5L6FHg
	en firebase se encuentras las keys requeridas
	
	***************************************************Compilar para generar e ir a PRD PUBLISH
	ng build --prod
	Cuando se genere la carpeta DIST:
	. va a generar un sub carpeta, esta no se requiere,sacar todo el contenido y colocarlo dentro de DIST
	
	npm install -g firebase-tools
	
	************************************************** Para despues de la 1 vez
	firebase login
	firebase init
	firebase deploy
	
	De prueba
	Test
	test@correo.com
	Test123*
	
	https://budgetapp-a96e9.web.app/
	
	************************************************
	https://reqres.in/
	
	
Teoria:{
	
	REDUX: {
		Patron para manejo de informacion
			- cual es estado de la app?
			- quien cambio el estado?
			- obtener los estados
			
		Tener en cuenta:
		- Toda la data de la aplicacions, se encuentra en una estructra previamente definida.
		- Toda la informacion se almacena en un unico lugar (STORE)
		- El Store JAMAS se modifica de forma directa.
		- Cualquier interaccion debe disparar acciones que describan que sucedio
		- El valor actual de la informacion de la app se llama (STATE)
		- Un nuevo estado es creado, con base a la combinacions del viejo estado + una accion, por medio del REDUCER.
		- NO MUTAR EL OBJ DE JS - Evitar la mutacion del obj
	}
	
	Action:{
	Actions describe unique events that are dispatched from components and services
		Es la unica fuente de informacion que se envia por interacciones del usuario o programa.
		Por lo general, se busca que las acciones sean lo mas simples posibles ATOMICAS.
		Propiedades:
			Type: Define que se quiere hacer
			Payload?: Informacion requeria para llevar a cabo la accion
	}
	
	Reducer:{
		Es una funcion unica que recibe (oldState y Action), siempre retorna un estado
		oldState: El estado actual de la aplicacion
		action: Es un OBJ plano que indica que hay que hacer. --simples
	}
	
	State:{
	State changes are handled by pure functions called reducers that take the current state and the latest action to compute a new state
		REGLAS:
		-	El state es de SOLO Lectura
		- 	Nunca se mutara el state de forma directa. INMUTABLE!!
		-	Funciones prohibidas JS : push - manipulacion directa del objeto
	}
	
	Store:{
		Es un Obj encargado de :
		-	Contener el estado actual de la app
		- Permite la lectura del estado: via getState[]
		- Permite crear un nuevo estado, usando: dispatch[ACTION]
		- Permite notificar cambios via subscribe [].
		- ES POSIBLE APLICAR LAZY-LOADING SOBRE EL STORE, ESTA ENE L GRUPO
		select:{
			para el Store, con esta propiedad se escuchan cambios especificos en el estado!
		}
	}
	
	Module:{
		El objetivo del module es encapsular
	}
	
	Desestructuracion :{
		[...obj] : saca cada uno de los item y los  regresa de forma independiente
		{...obj}: saca cada uno de los atributos y los  regresa de forma independiente
	}
	
	Subscribe:{
		Cuenta con diferentes funciones para valorar,transormar y ver los datos del observable, todas dentro del pipe:
		map:{
			Se usa para transformar el objeto que esta ene l observable
		}
		filter:{
			Filtra o valida, condiciona  si el observable trae la dara reqeurida para abrirse
		}
		
		take :{}
		
		
		tap:{}
		
		ofType:{}
		
		mergeMap:{}
		
		of:{
			Retornar observables!
		}
		
	}
	
	Effects:{
		No todas las accions deben aplicar Efectos (accions secundaria)
		
		Escucha Acciones para activar otro procedimiento - El resultado del Efecto es disparar una NUEVA ACCION que consumira el REDUCER
		Por lo cual, debe trabajar con la info que trae el  payload de la accion.
		
		Solo se activa con las acciones den ngrx/store
		Tiene como ideal hacer mas simple la logica de los componentes
		Obj: Comunicarse fuera del ecosistema de Angular (http,sockets o tareas asincronas)
		
	}
	
}

Errores:{
	
}
