import { Pipe, PipeTransform } from '@angular/core';
import { filtersTodo } from 'src/app/core/ngrx/actions/filter.actions';
import { Todo } from 'src/app/core/model/todo.model';

@Pipe({
  name: 'filterCustom',
})
export class FilterCustomPipe implements PipeTransform {
  transform(value: Todo[], filter: filtersTodo): Todo[] {
    switch (filter) {
      case 'completed':
        return value.filter((x) => x.complet == true);
      case 'pending':
        return value.filter((x) => x.complet == false);
      default:
        break;
    }
    return value;
  }
}
