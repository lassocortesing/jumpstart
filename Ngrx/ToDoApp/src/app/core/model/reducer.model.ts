import { ActionReducerMap } from '@ngrx/store';
import { stateModel } from './state.model';
import { todoReducer } from '../ngrx/reducers/todo.reducer';
import { filterReducer } from '../ngrx/reducers/filter.reducer';

export const reducerModel:ActionReducerMap<stateModel> = {
    todoActionstate:todoReducer,
    filterState:filterReducer
}