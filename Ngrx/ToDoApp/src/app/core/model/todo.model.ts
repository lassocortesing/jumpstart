export class Todo {
  public id: number;
  public text: string;
  public complet: boolean;
  constructor(newText: string) {
    this.text = newText;
    this.id = new Date().getTime();
    this.complet = false;
  }
}
