import { Todo } from './todo.model';
import { filtersTodo } from '../ngrx/actions/filter.actions';

export interface stateModel {
  todoActionstate: Todo[],
  filterState:filtersTodo
}
