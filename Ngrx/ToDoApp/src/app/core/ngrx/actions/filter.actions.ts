import { createAction, props } from '@ngrx/store';
export type filtersTodo = 'all' | 'completed' | 'pending';

export const allFilter = createAction('[FILTER] allFilter',props<{ filterType: filtersTodo }>());

