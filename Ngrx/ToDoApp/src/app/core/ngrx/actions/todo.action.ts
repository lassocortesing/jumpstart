import {createAction, props}from '@ngrx/store';

export const create = createAction('[TODO] create',props<{todoActionstate:string}>());
export const toggleChange = createAction('[TODO] toggleChange',props<{idItem:number}>());
export const editItem = createAction('[TODO] editItem',props<{idItem:number,newText:string}>());
export const deleteItem = createAction('[TODO] deleteItem',props<{idItem:number}>());
export const completeAll = createAction('[TODO] completeAll',props<{stateComplete:boolean}>());
export const clearAll = createAction('[TODO] clearAll');