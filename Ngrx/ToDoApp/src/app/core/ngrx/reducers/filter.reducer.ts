import { Action } from '@ngrx/store';
import { filtersTodo, allFilter } from '../actions/filter.actions';

const initialState: filtersTodo = 'all';
export function filterReducer(
  state: filtersTodo = initialState,
  action: Action
) {
  switch (action.type) {
    case allFilter.type:
      let getFilter = action['filterType'];
      state = getFilter;
      return state;

    default:
      return state;
  }
}
