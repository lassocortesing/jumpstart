import { Action } from '@ngrx/store';
import {
  create,
  toggleChange,
  editItem,
  deleteItem,
  completeAll,
  clearAll,
} from '../actions/todo.action';
import { Todo } from '../../model/todo.model';

const initialState: Todo[] = [new Todo('firstElement')];

export function todoReducer(state: Todo[] = initialState, action: Action) {
  switch (action.type) {
    case create.type:
      let createModel = new Todo(action['todoActionstate']);
      return [...state, createModel];

    case toggleChange.type:
      let id = action['idItem'];
      return state.map((newList) => {
        if (newList.id === id) {
          return { ...newList, complet: !newList.complet };
        } else {
          return newList;
        }
      });

    case editItem.type:
      let idItem = action['idItem'];
      let newText = action['newText'];
      return state.map((newList) => {
        if (newList.id === idItem) {
          return { ...newList, text: newText };
        } else {
          return newList;
        }
      });
    case deleteItem.type:
      let idDel = action['idItem'];
      return state.filter((del) => {
        if (del.id != idDel) {
          return del;
        }
      });

    case completeAll.type:
      let dataState = action['stateComplete'];
      return state.map((i) => {
        return { ...i, complet: dataState };
      });

    case clearAll.type:
      return state.filter((x) => x.complet == false);

    default:
      return state;
  }
}
