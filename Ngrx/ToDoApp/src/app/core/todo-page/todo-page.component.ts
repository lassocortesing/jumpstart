import { Component, OnInit } from '@angular/core';
import { stateModel } from '../model/state.model';
import { Store } from '@ngrx/store';
import { completeAll } from '../ngrx/actions/todo.action';

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.css'],
})
export class TodoPageComponent implements OnInit {
  completeAll: boolean = false;
  constructor(private store: Store<stateModel>) {}

  ngOnInit(): void {}

  changeAll() {
    this.completeAll = !this.completeAll;
    this.store.dispatch(completeAll({ stateComplete: this.completeAll }));
  }
}
