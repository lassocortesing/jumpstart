import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { stateModel } from '../model/state.model';
import { Store } from '@ngrx/store';
import * as myActions from '../ngrx/actions/todo.action';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css'],
})
export class TodoAddComponent implements OnInit {
  txtInputParam: FormControl;
  constructor(private store: Store<stateModel>) {
    this.txtInputParam = new FormControl('', Validators.required);
  }

  ngOnInit(): void {}
  addItem() {
    if (this.txtInputParam.valid) {
      this.store.dispatch(
        myActions.create({ todoActionstate: this.txtInputParam.value })
      );
      this.txtInputParam.reset();
    }
  }
}
