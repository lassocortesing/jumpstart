import { Component, OnInit } from '@angular/core';
import { Todo } from '../model/todo.model';
import { stateModel } from '../model/state.model';
import { Store } from '@ngrx/store';
import { filtersTodo } from '../ngrx/actions/filter.actions';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
})
export class TodoListComponent implements OnInit {
  todoItems: Todo[] = [];
  getFilter: filtersTodo;

  constructor(private store: Store<stateModel>) {
    this.store.subscribe((result) => {
      this.todoItems = result.todoActionstate;
      this.getFilter = result.filterState;
    });
  }

  ngOnInit(): void {}
}
