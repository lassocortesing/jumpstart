import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Todo } from '../model/todo.model';
import { FormControl, Validators } from '@angular/forms';
import { stateModel } from '../model/state.model';
import { Store } from '@ngrx/store';
import {
  toggleChange,
  editItem,
  deleteItem,
} from '../ngrx/actions/todo.action';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css'],
})
export class TodoItemComponent implements OnInit {
  @Input() paramTodo: Todo;
  chCompleted: FormControl;
  txtInput: FormControl;
  isEdit: boolean = false;
  @ViewChild('refInput') txtRefInput: ElementRef;
  constructor(private store: Store<stateModel>) {}

  ngOnInit(): void {
    this.chCompleted = new FormControl(this.paramTodo.complet);
    this.txtInput = new FormControl(this.paramTodo.text, Validators.required);
    this.chCompleted.valueChanges.subscribe((eventChk) => {
      this.store.dispatch(toggleChange({ idItem: this.paramTodo.id }));
    });
  }
  editItem() {
    this.isEdit = true;
    setTimeout(() => {
      this.txtRefInput.nativeElement.focus();
    }, 1);
  }
  blurFoco() {
    this.isEdit = false;

    if (this.txtInput.invalid) return;

    this.store.dispatch(
      editItem({
        idItem: this.paramTodo.id,
        newText: this.txtInput.value,
      })
    );
  }

  delItem() {
    this.store.dispatch(deleteItem({ idItem: this.paramTodo.id }));
  }
}
