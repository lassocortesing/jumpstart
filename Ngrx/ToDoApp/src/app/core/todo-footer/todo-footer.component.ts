import { Component, OnInit } from '@angular/core';
import { stateModel } from '../model/state.model';
import { Store } from '@ngrx/store';
import { filtersTodo, allFilter } from '../ngrx/actions/filter.actions';
import { clearAll } from '../ngrx/actions/todo.action';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styleUrls: ['./todo-footer.component.css'],
})
export class TodoFooterComponent implements OnInit {
  applyFilter: filtersTodo = 'all';
  filters: filtersTodo[] = ['all', 'completed', 'pending'];
  pendingItem: number = 0;
  constructor(private store: Store<stateModel>) {}

  ngOnInit(): void {
    this.store.subscribe((result) => {
      this.applyFilter = result.filterState;

      this.pendingItem = result.todoActionstate.filter(
        (x) => x.complet == false
      ).length;
    });
  }

  filterSelected(filterSel: filtersTodo) {
    this.store.dispatch(allFilter({ filterType: filterSel }));
  }
  clearCompleted() {
    this.store.dispatch(clearAll());
  }
}
