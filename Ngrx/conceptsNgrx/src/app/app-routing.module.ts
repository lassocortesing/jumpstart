import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasicComponent } from './components/basic/basic.component';
import { HomeFunComponent } from './components/fundamental/home-fun/home-fun.component';

const routes: Routes = [
  { path: 'base', component: BasicComponent },
  { path: 'fundamental', component: HomeFunComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
