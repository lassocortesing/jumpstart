import { Component, OnInit } from '@angular/core';
import { operationsReducer } from './reducer/operations.reducer';
import {
  incrementarAction,
  decrementarAction,
  multiplicarAction,
  dividirAction,
  resetAction,
} from './action/operations.actions';
import { myStore } from './ngrxFake/store';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css'],
})
export class BasicComponent implements OnInit {
  constructor() {
    this.casoBase();
    this.addStore();
  }

  ngOnInit(): void {}

  casoBase() {
    console.log(
      ' Reducer-incrementar',
      operationsReducer(10, incrementarAction)
    );

    console.log(
      ' Reducer-decrementar',
      operationsReducer(10, decrementarAction)
    );

    console.log(
      ' Reducer-multiplicar',
      operationsReducer(10, multiplicarAction)
    );

    console.log(' Reducer - dividir', operationsReducer(10, dividirAction));

    console.log(' Reducer - reset', operationsReducer(10, resetAction));
  }

  addStore() {
    const store = new myStore(operationsReducer, 10);
    console.log('initial State: ', store.getState());
    store.dispatchAction(incrementarAction);
    console.log('State after increment: ', store.getState());
    store.dispatchAction(multiplicarAction);
    console.log('State after multiplicar: ', store.getState());
  }
}
