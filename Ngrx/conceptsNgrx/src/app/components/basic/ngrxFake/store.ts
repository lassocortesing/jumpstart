import { MyReducer, MyAction } from './ngrx';

export class myStore<T> {
  constructor(private reducer: MyReducer<T>, private state: T) {}

  getState() {
    return this.state;
  }

  dispatchAction(act: MyAction) {
    this.state = this.reducer(this.state, act);
  }
}
