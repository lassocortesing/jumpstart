export interface MyAction {
  type: string;
  payload?: any;
}

export interface MyReducer<T> {
  (state: T, actino: MyAction): T;
}
