import { MyAction } from '../ngrxFake/ngrx';

export function operationsReducer(state = 2, action: MyAction) {
  switch (action.type) {
    case 'incrementar':
      return (state += 1);

    case 'decrementar':
      return (state -= 1);

    case 'multiplicar':
      return (state *= action.payload);

    case 'dividir':
      return (state /= action.payload);

    case 'reset':
      return (state = 0);
    default:
      return state;
  }
}
