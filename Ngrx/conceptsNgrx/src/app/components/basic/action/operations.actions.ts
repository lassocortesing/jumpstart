import { MyAction } from '../ngrxFake/ngrx';

export const incrementarAction: MyAction = {
  type: 'incrementar',
};

export const decrementarAction: MyAction = {
  type: 'decrementar',
};

export const multiplicarAction: MyAction = {
  type: 'multiplicar',
  payload: 2,
};

export const dividirAction: MyAction = {
  type: 'dividir',
  payload: 2,
};

export const resetAction: MyAction={
  type:'reset'
}
