import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { operarState } from '../../model/iOpera.model';
import { inc, dec } from '../ngrxReducer/actions/operation.action';

@Component({
  selector: 'app-home-fun',
  templateUrl: './home-fun.component.html',
  styleUrls: ['./home-fun.component.css'],
})
export class HomeFunComponent implements OnInit {
  operation: number;
  ngOperation: number;
  constructor(private store: Store<operarState>) {
    this.operation = 10;

    // this.store.subscribe((result) => {this.ngOperation = result.operar;});
    this.store.select('operar').subscribe((result) => {
      this.ngOperation = result;
    });
  }

  ngOnInit(): void {}

  incrementar() {
    this.operation += 1;
  }
  decrementar() {
    this.operation -= 1;
  }

  ri() {
    this.store.dispatch(inc());
  }

  rd() {
    this.store.dispatch(dec());
  }
}
