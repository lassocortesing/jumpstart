import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { operarState } from '../../model/iOpera.model';
import { Store } from '@ngrx/store';
import { mul, div } from '../ngrxReducer/actions/operation.action';
@Component({
  selector: 'app-first-fun',
  templateUrl: './first-fun.component.html',
  styleUrls: ['./first-fun.component.css'],
})
export class FirstFunComponent implements OnInit {
  @Input() param: number;
  @Output() cambioOut = new EventEmitter<number>();
  ngOperation: number;
  constructor(private store: Store<operarState>) {
    this.store.select('operar').subscribe((result) => {
      this.ngOperation = result;
    });
  }

  ngOnInit(): void {}
  mult() {
    this.param *= 2;
    this.cambioOut.emit(this.param);
  }
  divi() {
    this.param /= 2;
    this.cambioOut.emit(this.param);
  }
  propagarCambio(twoCambio: number) {
    this.param = twoCambio;
    this.cambioOut.emit(this.param);
  }

  rm() {
    this.store.dispatch(mul({ num: 3 }));
  }
  rd() {
    this.store.dispatch(div({ num: 3 }));
  }
}
