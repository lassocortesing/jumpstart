import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstFunComponent } from './first-fun.component';

describe('FirstFunComponent', () => {
  let component: FirstFunComponent;
  let fixture: ComponentFixture<FirstFunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstFunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstFunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
