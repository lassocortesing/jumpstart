import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { res } from '../ngrxReducer/actions/operation.action';
import { Store } from '@ngrx/store';
import { operarState } from '../../model/iOpera.model';
@Component({
  selector: 'app-second-fun',
  templateUrl: './second-fun.component.html',
  styleUrls: ['./second-fun.component.css'],
})
export class SecondFunComponent implements OnInit {
  @Input() paramTwo: number;
  @Output() twoCambioOut = new EventEmitter<number>();
  ngOperation: number;
  constructor(private store: Store<operarState>) {
    this.store.select('operar').subscribe((result) => {
      this.ngOperation = result;
    });
  }

  ngOnInit(): void {}
  reset() {
    this.paramTwo = 0;
    this.twoCambioOut.emit(0);
  }
  rs() {
    this.store.dispatch(res());
  }
}
