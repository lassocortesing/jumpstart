import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondFunComponent } from './second-fun.component';

describe('SecondFunComponent', () => {
  let component: SecondFunComponent;
  let fixture: ComponentFixture<SecondFunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecondFunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondFunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
