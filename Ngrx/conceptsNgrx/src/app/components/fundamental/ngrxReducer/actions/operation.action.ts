import { createAction, props } from '@ngrx/store';

export const inc = createAction('[operar] incrementar');
export const dec = createAction('[operar] decrementar');
export const mul = createAction('[operar] multiplicar', props<{ num: number }>());
export const div = createAction('[operar] dividir',props<{ num: number }>());
export const res = createAction('[operar] reiniciar');
