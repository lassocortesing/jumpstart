import { Action, props } from '@ngrx/store';
import { operarState } from 'src/app/components/model/iOpera.model';
import { inc, dec, mul, div, res } from '../actions/operation.action';

const initial: operarState = { operar: 10 };
export function operationReducer(state: number = 10, action: Action) {
  switch (action.type) {
    case inc.type:
      state += 1;
      return state;
    case dec.type:
      state -= 1;
      return state;
    case mul.type:
      let payloadM = action['num'];
      state *= payloadM;
      return state;
    case div.type:
      let payloadD = action['num'];
      state /= payloadD;
      return state;
    case res.type:
      state = 0;
      return state;
    default:
      return state;
  }
}
