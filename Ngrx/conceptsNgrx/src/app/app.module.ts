import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BasicComponent } from './components/basic/basic.component';
import { HomeFunComponent } from './components/fundamental/home-fun/home-fun.component';
import { FirstFunComponent } from './components/fundamental/first-fun/first-fun.component';
import { SecondFunComponent } from './components/fundamental/second-fun/second-fun.component';
import { HeaderComponent } from './shared/header/header.component';

import { StoreModule } from '@ngrx/store';
import { operationReducer } from './components/fundamental/ngrxReducer/reducers/operation.reducer';
import { StoreDevtools, StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    BasicComponent,
    HomeFunComponent,
    FirstFunComponent,
    SecondFunComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({ operar: operationReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
