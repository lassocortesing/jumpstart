import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUsersComponent } from './core/users/list-users/list-users.component';
import { UserComponent } from './core/users/user/user.component';

const routes: Routes = [
  { path: 'home', component: ListUsersComponent },
  { path: 'user/:id', component: UserComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
