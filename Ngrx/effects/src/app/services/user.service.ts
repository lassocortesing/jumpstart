import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private urlBase = 'https://reqres.in/api/';
  constructor(private http: HttpClient) {}

  getListUsers(): Observable<any> {
    return this.http.get(`${this.urlBase}users?per_page=6`).pipe(
      map((rm) => {
        return rm['data'];
      })
    );
  }

  getUser(id: string) {
    return this.http.get(`${this.urlBase}users/${id}`).pipe(
      map((rm) => {
        return rm['data'];
      })
    );
  }
}
