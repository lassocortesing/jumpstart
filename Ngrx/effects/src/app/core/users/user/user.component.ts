import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminState } from '../../ngrx/states/admin.state';
import { Store } from '@ngrx/store';
import { loadTheUserAction } from '../../ngrx/actions';
import { Subscription } from 'rxjs';
import { ErrorModel } from 'src/app/models/error.model';
import { User } from 'src/app/models/user.model';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit, OnDestroy {
  userDubs: Subscription;

  userWeb: User = { avatar: '', first_name: '', last_name: '', id: '' };
  loading: boolean = false;
  error: ErrorModel = { message: '', name: '', url: '' };

  constructor(
    private router: ActivatedRoute,
    private store: Store<AdminState>
  ) {}
  ngOnDestroy(): void {
    this.userDubs.unsubscribe();
  }

  ngOnInit(): void {
    this.router.params.subscribe(({ id }) => {
      this.store.dispatch(loadTheUserAction({ payload: id }));

      this.userDubs = this.store
        .select('userActions')
        .pipe(
          filter((rf) => {
            return (
              rf &&
              rf != null &&
              rf != undefined &&
              rf.user != null &&
              rf.user != undefined
              // && rf.lstUsers.length != 0
            );
          })
        )
        .subscribe(({ user, loading, error }) => {
          this.userWeb = user;
          this.loading = loading;
          this.error = error;
        });
    });
  }
}
