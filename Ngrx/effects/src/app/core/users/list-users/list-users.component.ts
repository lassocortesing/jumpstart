import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { map, filter } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';
import { AdminState } from '../../ngrx/states/admin.state';
import { Store } from '@ngrx/store';
import { loadUsersAction } from '../../ngrx/actions';
import { Subscription } from 'rxjs';
import { ErrorModel } from 'src/app/models/error.model';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css'],
})
export class ListUsersComponent implements OnInit, OnDestroy {
  actionEffectSub: Subscription;
  listUsersWeb: User[] = [];  
  loading: boolean = false;
  error: ErrorModel = { message: '', name: '', url: '' };

  constructor(private store: Store<AdminState>) {}
  ngOnDestroy(): void {
    this.actionEffectSub.unsubscribe();
  }

  ngOnInit(): void {
    this.store.dispatch(loadUsersAction());
    this.actionEffectSub = this.store
      .select('usersActions')
      .pipe(
        filter((rf) => {
          return (
            rf &&
            rf != null &&
            rf != undefined &&
            rf.lstUsers != null &&
            rf.lstUsers != undefined
            // && rf.lstUsers.length != 0
          );
        })
      )
      .subscribe(({ lstUsers, loading, error }) => {
        this.listUsersWeb = lstUsers;
        this.loading = loading;
        this.error = error;
      });
  }
}
