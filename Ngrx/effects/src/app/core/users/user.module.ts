import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListUsersComponent } from './list-users/list-users.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [ListUsersComponent, UserComponent],
  imports: [CommonModule],
  exports: [ListUsersComponent, UserComponent],
})
export class UserModule {}
