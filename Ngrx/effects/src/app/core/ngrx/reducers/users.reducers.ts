import { createReducer, Action } from '@ngrx/store';
import { usersState } from '../states/users.state';
import { User } from 'src/app/models/user.model';
import {
  loadUsersAction,
  loadUsersFailAction,
  loadUsersOkAction,
} from '../actions';

export const initialState: usersState = {
  lstUsers: [],
  loaded: false,
  loading: false,
  error: null,
};
export function usersReducer(state: usersState = initialState, action: Action) {
  switch (action.type) {
    case loadUsersAction.type:
      return { ...state, loading: true };

    case loadUsersOkAction.type:
      return {
        ...state,
        loading: false,
        loaded: true,
        lstUsers: [...action['payload']],
      };

    case loadUsersFailAction.type:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: {
          url: action['payload'].url,
          name: action['payload'].name,
          message: action['payload'].message,
        },
      };
    default:
      return state;
  }
}
