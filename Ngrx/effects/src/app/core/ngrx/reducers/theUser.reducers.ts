import { createReducer, Action } from '@ngrx/store';
import { theUserState } from '../states/theUser.state';
import {
  loadTheUserAction,
  loadTheUserFailAction,
  loadTheUserOkAction,
} from '../actions';

export const initialUserState: theUserState = {
  id: '',
  user: null,
  loaded: false,
  loading: false,
  error: null,
};
export function theUserReducer(
  state: theUserState = initialUserState,
  action: Action
) {
  switch (action.type) {
    case loadTheUserAction.type:
      return { ...state, loading: true, id: action['payload'] };

    case loadTheUserOkAction.type:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: { ...action['payload'] },
      };

    case loadTheUserFailAction.type:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: {
          ...action['payload'],
          //   url: action['payload'].url,
          //   name: action['payload'].name,
          //   message: action['payload'].message,
        },
      };
    default:
      return state;
  }
}
