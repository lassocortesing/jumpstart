import { ActionReducerMap } from '@ngrx/store';
import { AdminState } from '../states/admin.state';
import { usersReducer, theUserReducer } from '../reducers';

export const AdminReducers: ActionReducerMap<AdminState> = {
  usersActions: usersReducer,
  userActions: theUserReducer,
};
