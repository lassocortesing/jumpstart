import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as theUsersActions from '../actions/theUser.actions';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { of } from 'rxjs';

@Injectable()
export class theUserEffects {
  constructor(private action$: Actions, private useService: UserService) {}

  loadUsersEffects = createEffect(() =>
    this.action$.pipe(
      ofType(theUsersActions.loadTheUserAction),
      mergeMap((action) =>
        this.useService.getUser(action.payload).pipe(
          map((result: User) => {
            return theUsersActions.loadTheUserOkAction({ payload: result });
          }),
          catchError((fail) => {
            return of(theUsersActions.loadTheUserOkAction({ payload: fail }));
          })
        )
      )
    )
  );
}
