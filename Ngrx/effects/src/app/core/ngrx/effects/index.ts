import { usersEffects } from './users.effects';
import { theUserEffects } from './user.effects';

export const EffectsArray: any[] = [usersEffects, theUserEffects];
