import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as usersActions from '../actions/users.actions';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
import { of } from 'rxjs';

@Injectable()
export class usersEffects {
  constructor(private action$: Actions, private useService: UserService) {}

  loadUsersEffects = createEffect(() =>
    this.action$.pipe(
      ofType(usersActions.loadUsersAction),
      mergeMap(() =>
        this.useService.getListUsers().pipe(
          map((result: User[]) => {
            return usersActions.loadUsersOkAction({ payload: result });
          }),
          catchError((fail) => {
            return of(usersActions.loadUsersFailAction({ payload: fail }));
          })
        )
      )
    )
  );
}
