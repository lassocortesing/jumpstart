import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/models/user.model';
import { ErrorModel } from 'src/app/models/error.model';

export const loadUsersAction = createAction('[Users] loadUsersAction');

export const loadUsersOkAction = createAction(
  '[Users] loadUsersOkAction',
  props<{ payload: User[] }>()
);
export const loadUsersFailAction = createAction(
  '[Users] loadUsersFailAction',
  props<{ payload: ErrorModel }>()
);
