import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/models/user.model';
import { ErrorModel } from 'src/app/models/error.model';

export const loadTheUserAction = createAction(
  '[User] loadTheUserAction',
  props<{ payload: string }>()
);

export const loadTheUserOkAction = createAction(
  '[User] loadTheUserOkAction',
  props<{ payload: User }>()
);
export const loadTheUserFailAction = createAction(
  '[User] loadTheUserFailAction',
  props<{ payload: ErrorModel }>()
);
