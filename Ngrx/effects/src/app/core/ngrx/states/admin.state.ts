import { usersState } from '../states/users.state';
import { theUserState } from './theUser.state';
export interface AdminState {
  usersActions: usersState;
  userActions: theUserState;
}
