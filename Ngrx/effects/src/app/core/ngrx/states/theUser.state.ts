import { User } from 'src/app/models/user.model';
import { ErrorModel } from 'src/app/models/error.model';

export interface theUserState {
  id: string;
  user: User;
  loaded: boolean;
  loading: boolean;
  error: ErrorModel;
}
