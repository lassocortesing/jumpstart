export interface ErrorModel {
  url: string;
  name: string;
  message: string;
}
